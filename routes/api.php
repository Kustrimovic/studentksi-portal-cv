<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/authorization', 'UserController@authorization');

Route::resource('/posts', 'PostController',
    ['only' => ['index', 'store', 'show']]);
Route::post('/posts/update/{id}', 'PostController@update');
Route::post('/posts/delete/{id}', 'PostController@destroy');
Route::post('/posts/search', 'PostController@search');

Route::resource('/courses', 'CourseController',
    ['only' => ['index', 'store', 'show']]);
Route::post('/courses/update/{id}', 'CourseController@update');
Route::post('/courses/delete/{id}', 'CourseController@destroy');
Route::post('/courses/complete/{id}', 'CourseController@complete');
Route::get('/my-courses', 'CourseController@myCourses');
Route::post('/courses/search', 'CourseController@search');
Route::post('/courses/search-my-courses', 'CourseController@searchMyCourses');
Route::post('/apply-course/{id}', 'CourseController@applyCourse');
Route::post('/remove-applicant/{id}', 'CourseController@removeApplicant');
Route::post('/add-applicant/{id}', 'CourseController@addApplicant');

Route::resource('/students', 'StudentController',
    ['only' => ['index', 'store', 'show']]);
Route::post('/students/update/{id}', 'StudentController@update');
Route::post('/students/delete/{id}', 'StudentController@destroy');
Route::get('/role/students', 'StudentController@getStudentsNoTeachers');
Route::get('/course-add/students/{id}', 'StudentController@getStudentsAddToCourse');
Route::post('/students/search', 'StudentController@search');

Route::resource('/companies', 'CompanyController',
    ['only' => ['index', 'store', 'show']]);
Route::post('/companies/update/{id}', 'CompanyController@update');
Route::post('/companies/delete/{id}', 'CompanyController@destroy');
Route::post('/companies/search', 'CompanyController@search');

Route::resource('/notice', 'AdController',
    ['only' => ['index', 'store', 'show']]);
Route::post('/notice/update/{id}', 'AdController@update');
Route::post('/notice/delete/{id}', 'AdController@destroy');
Route::get('/company-notice', 'AdController@companyAds');
Route::post('/notice/search', 'AdController@search');
Route::post('/apply-notice/{id}', 'AdController@applyAd');
Route::post('/my-applications', 'AdController@studentAdsApplications');

Route::resource('/suggestions', 'SuggestionController',
    ['only' => ['index', 'store']]);
Route::post('/suggestions/update/{id}', 'SuggestionController@update');
Route::post('/suggestions/delete/{id}', 'SuggestionController@destroy');

Route::resource('/certificates', 'CertificateController',
    ['only' => ['index', 'store', 'show']]);
Route::post('/certificates/update/{id}', 'CertificateController@update');
Route::post('/certificates/delete/{id}', 'CertificateController@destroy');
Route::post('/certificates/search', 'CertificateController@search');
Route::get('/allcertificates', 'CertificateController@allCertificates');

Route::get('/professors/all', 'ProfessorController@getMainProfessors');

Route::resource('/teachers', 'TeacherController',
    ['only' => ['index', 'store']]);
Route::post('/teachers/remove/{id}', 'TeacherController@destroy');
Route::get('/allteachers', 'TeacherController@allTeachers');

Route::resource('/admins', 'AdminController',
    ['only' => ['index', 'store']]);
Route::post('/admins/remove/{id}', 'AdminController@destroy');

Route::get('/contact-email', 'ContactController@index');
Route::post('/contact-email/update', 'ContactController@update');
Route::post('/contact-us', 'ContactController@contactUs');
