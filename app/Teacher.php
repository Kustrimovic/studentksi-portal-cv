<?php

namespace App;

use App\Helpers\Constant;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        'teacherable_id', 'teacherable_type'
    ];

    public function teacherable()
    {
        return $this->morphTo();
    }

    public function courses()
    {
        return $this->hasMany('App\Course');
    }

    public function suggestions()
    {
        return $this->morphMany('App\Suggestion', 'suggestionable');
    }

    public function delete() {
        $courses = Course::where('teacher_id', $this->id)->get();
        foreach ($courses as $course)
        {
            $course->delete();
        }

        $suggestions = Suggestion::where([['suggestionable_id', $this->id], ['suggestionable_type', Constant::TEACHER_IDENTIFIER]])->get();
        foreach ($suggestions as $suggestion)
        {
            $suggestion->delete();
        }

        return parent::delete();
    }
}
