<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;

class CreateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'string|required',
            'last_name' => 'string|required',
            'date_of_birth' => 'date|required',
            'email' => 'unique:students|email|required',
            'city' => 'string|required',
            'education' => 'string',
            'employment' => 'string',
            'workplace' => 'string',
            'linkedin_url' => 'string'
        ];
    }

    public function response(array $errors)
    {
        return response()->custom(400, $errors, null);
    }
}
