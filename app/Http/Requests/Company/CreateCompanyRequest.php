<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class CreateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'unique:companies|string|required',
            'password' => 'string|required',
            'name' => 'string|required',
            'lastname' => 'string|required',
            'email' => 'unique:companies|email|required',
            'company_name' => 'required'
        ];
    }

    public function response(array $errors)
    {
        return response()->custom(400, $errors, null);
    }
}
