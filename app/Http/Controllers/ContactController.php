<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Helpers\Constant;
use App\Helpers\MainModuleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->token, false);
            if(!$response['error'])
            {
                $contactMail = Contact::first();
                return response()->custom(200, Constant::SUCCESS_GET_LIST_EMAILS_MESSAGE, $contactMail);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->token, false);
            if(!$response['error'])
            {
                $contactMail = Contact::first();
                if($request->email) {
                    $contactMail->email = $request->email;
                    $contactMail->save();
                }
                return response()->custom(200, Constant::SUCCESS_EDIT_MESSAGE, Contact::first());
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }

    public function contactUs(Request $request)
    {
        $contactMail = Contact::first();
        Mail::send([], [], function ($message) use ($request, $contactMail) {
            $message->to($contactMail->email)
                ->subject($request->subject)
                ->from($request->email)
                ->setBody($request->body);
        });

        return response()->custom(200, Constant::SUCCESS_CONTACT_MESSAGE, null);
    }
}
