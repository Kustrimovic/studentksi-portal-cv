<?php

namespace App\Http\Controllers;

use App\Certificate;
use App\Helpers\Constant;
use App\Helpers\MainModuleService;
use App\Http\Requests\Certificate\CreateCertificateRequest;
use App\Http\Requests\Certificate\UpdateCertificateRequest;
use Illuminate\Http\Request;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $certificates = Certificate::latest()->paginate(10);
                if(count($certificates)) {
                    return response()->custom(200, Constant::SUCCESS_GET_CERTIFICATES_MESSAGE, $certificates);
                }
                return response()->custom(200, Constant::SUCCESS_GET_CERTIFICATES_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCertificateRequest $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin' || $response['body']['entity']['role'] == 'teacher')
                {
                    $certificate = new Certificate();
                    $certificate->fill($request->all());
                    if($certificate->save())
                    {
                        return response()->custom(200, Constant::SUCCESS_CREATED_CERTIFICATE_MESSAGE, $certificate);
                    }
                    return response()->custom(400, Constant::ERROR_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $certificate = Certificate::find($id);
                if($certificate) {
                    return response()->custom(200, Constant::SUCCESS_GET_CERTIFICATE_MESSAGE, $certificate);
                }
                return response()->custom(404, Constant::CERTIFICATE_NOT_FOUND_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function edit(Certificate $certificate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCertificateRequest $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin' || $response['body']['entity']['role'] == 'teacher')
                {
                    $certificate = Certificate::find($id);
                    if($certificate)
                    {
                        $certificate->fill($request->all());
                        if($certificate->save())
                        {
                            return response()->custom(200, Constant::SUCCESS_EDIT_MESSAGE, $certificate);
                        }
                        return response()->custom(400, Constant::ERROR_MESSAGE, null);
                    }
                    return response()->custom(404, Constant::CERTIFICATE_NOT_FOUND_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin' || $response['body']['entity']['role'] == 'teacher')
                {
                    if(Certificate::destroy($id))
                    {
                        return response()->custom(200, Constant::SUCCESS_DELETE_MESSAGE, null);
                    }
                    return response()->custom(400, Constant::ERROR_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function allCertificates(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $certificates = Certificate::latest()->get();
                if(count($certificates)) {
                    return response()->custom(200, Constant::SUCCESS_GET_CERTIFICATES_MESSAGE, $certificates);
                }
                return response()->custom(200, Constant::SUCCESS_GET_CERTIFICATES_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function search(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $certificates = Certificate::Search($request->keyword)->latest()->paginate(10);
                if(count($certificates)) {
                    return response()->custom(200, Constant::SUCCESS_GET_CERTIFICATES_MESSAGE, $certificates);
                }
                $data['data'] = null;
                return response()->custom(200, Constant::SUCCESS_GET_CERTIFICATES_MESSAGE, $data);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }
}
