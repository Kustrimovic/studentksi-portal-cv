<?php

namespace App\Http\Controllers;

use App\Course;
use App\Helpers\Constant;
use App\Helpers\ImageService;
use App\Helpers\MainModuleService;
use App\Http\Requests\Student\CreateStudentRequest;
use App\Http\Requests\Student\UpdateStudentRequest;
use App\Image;
use App\Student;
use App\Teacher;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $students = Student::with('image', 'courses', 'certificates', 'suggestions')->latest()->paginate(10);
                if(count($students)) {
                    return response()->custom(200, Constant::SUCCESS_GET_STUDENTS_MESSAGE, $students);
                }
                return response()->custom(200, Constant::SUCCESS_GET_STUDENTS_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStudentRequest $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), true);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'student')
                {
                    $student = Student::where('user_id', $response['body']['entity']['id'])->first();
                    if(!$student)
                    {
                        $student = new Student();
                        $student->fill($request->all());
                        $student->user_id = $response['body']['entity']['id'];
                        if($student->save()) {
                            $profile_img = new Image();
                            $profile_img->imageable_id = $student->id;
                            $profile_img->imageable_type = Constant::STUDENT_IDENTIFIER;
                            if($request->hasFile('profile_img')) {
                                $profile_img->path = ImageService::upload($request->file('profile_img'), Constant::USER_IMAGES_ROOT_PATH, $student->id, $request->file('profile_img')->getClientOriginalName());
                            } else {
                                $profile_img->path = Constant::DEFAULT_USER_IMAGE_PATH;
                            }
                            $profile_img->save();
                            return response()->custom(200, Constant::SUCCESS_CREATED_STUDENT_CV_MESSAGE, Student::with('image', 'courses', 'certificates', 'suggestions')->find($student->id));
                        }
                        return response()->custom(400, Constant::ERROR_MESSAGE, null);
                    }
                    return response()->custom(400, Constant::ALREADY_CREATED_STUDENT_CV_MESSAGE, Student::where('user_id', $response['body']['entity']['id'])->with('image', 'courses', 'certificates', 'suggestions')->first());
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $student = Student::with('image', 'courses', 'certificates', 'suggestions')->find($id);
                if($student) {
                    $studentName = $student->first_name . '_' . $student->last_name . '_CV';
                    $path = 'pdfs/'.$studentName.'.pdf';
                    $student->date = \Carbon\Carbon::now()->format('d-m-Y');
                    PDF::loadView('pdf.invoice', compact('student', $student))->save(public_path().'/'.$path);
                    $student->pdf_route = $path;
                    return response()->custom(200, Constant::SUCCESS_GET_STUDENT_MESSAGE, $student);
                }
                return response()->custom(404, Constant::STUDENT_NOT_FOUND_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudentRequest $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['id'] == $id && ($response['body']['entity']['role'] == 'student' || $response['body']['entity']['role'] == 'teacher'))
                {
                    $student = Student::find($id);
                    if($student)
                    {
                        $student->fill($request->all());
                        if($request->hasFile('profile_img')) {
                            $profile_img = Image::where([['imageable_id', '=', $student->id], ['imageable_type', '=', Constant::STUDENT_IDENTIFIER]])->first();
                            if($profile_img && $profile_img->path != Constant::DEFAULT_USER_IMAGE_PATH) {
                                ImageService::remove($profile_img->path, Constant::USER_IMAGES_ROOT_PATH, $student->id);
                            }
                            $profile_img->path = ImageService::upload($request->file('profile_img'), Constant::USER_IMAGES_ROOT_PATH, $student->id, $request->file('profile_img')->getClientOriginalName());
                            $profile_img->save();
                        }
                        $student->save();
                        return response()->custom(200, Constant::SUCCESS_EDIT_MESSAGE, Student::with('image', 'courses', 'certificates', 'suggestions')->find($student->id));
                    }
                    return response()->custom(404, Constant::STUDENT_NOT_FOUND_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if(($response['body']['entity']['id'] == $id && $response['body']['entity']['role'] == 'student' || $response['body']['entity']['role'] == 'teacher') || $response['body']['entity']['role'] == 'admin')
                {
                    $student = Student::find($id);
                    if($student && $student->delete())
                    {
                        return response()->custom(200, Constant::SUCCESS_DELETED_STUDENT_CV_MESSAGE, null);
                    }
                    return response()->custom(404, Constant::STUDENT_NOT_FOUND_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function getStudentsNoTeachers(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $students = Student::with('image', 'courses', 'certificates', 'suggestions')->where('role', 'student')->latest()->paginate(10);
                if(count($students))
                {
                    return response()->custom(200, Constant::SUCCESS_GET_STUDENTS_ROLE_STUDENT_MESSAGE, $students);
                }
                return response()->custom(200, Constant::STUDENT_WITH_ROLE_STUDENT_NOT_FOUND_MESSAGE);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function getStudentsAddToCourse(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'teacher' && $response['body']['entity']['model'] == Constant::STUDENT_IDENTIFIER) {
                    $students = Student::where('id', '!=', $response['body']['entity']['id'])->whereDoesntHave('courses', function ($q) use ($id) {
                        $q->where('course_id', $id);
                    })->latest()->paginate(10);
                } else {
                    $course = Course::where('id', $id)->first();
                    if($course->teacher->teacherable_type == Constant::STUDENT_IDENTIFIER) {
                        $students = Student::where('id', '!=', $course->teacher->teacherable_id)->whereDoesntHave('courses', function ($q) use ($id) {
                            $q->where('course_id', $id);
                        })->latest()->paginate(10);
                    } else {
                        $students = Student::whereDoesntHave('courses', function ($q) use ($id) {
                            $q->where('course_id', $id);
                        })->latest()->paginate(10);
                    }
                }
                if(count($students))
                {
                    return response()->custom(200, Constant::SUCCESS_GET_STUDENTS_CAN_LISTEN_COURSE_MESSAGE, $students);
                }
                return response()->custom(200, Constant::STUDENT_CAN_LISTEN_COURSE_NOT_FOUND_MESSAGE);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function search(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($request->has('certificate_id'))
                {
                    $id = $request->certificate_id;
                    $students = Student::Search($request->keyword)->with('image', 'courses', 'certificates', 'suggestions')->whereHas('certificates', function ($q) use ($id) {
                        $q->where('certificate_id', $id);
                    })->latest()->paginate(10);
                } else {
                    $students = Student::Search($request->keyword)->with('image', 'courses', 'certificates', 'suggestions')->latest()->paginate(10);
                }
                if(count($students)) {
                    return response()->custom(200, Constant::SUCCESS_GET_STUDENTS_MESSAGE, $students);
                }
                $data['data'] = null;
                return response()->custom(200, Constant::SUCCESS_GET_STUDENTS_MESSAGE, $data);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }
}
