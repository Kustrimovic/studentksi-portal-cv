<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\ImageService;
use App\Helpers\MainModuleService;
use App\Http\Requests\Post\CreatePostRequest;
use App\Http\Requests\Post\UpdatePostRequest;
use App\Image;
use App\Post;
use App\Posts;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $posts = Post::with('images')->latest()->paginate(10);
                if(count($posts)) {
                    return response()->custom(200, Constant::SUCCESS_GET_POSTS_MESSAGE, $posts);
                }
                return response()->custom(200, Constant::SUCCESS_GET_POSTS_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin')
                {
                    $post = new Post();
                    $post->fill($request->all());
                    if($post->save()) {
                        if($request->hasFile('image')) {
                            $image = new Image();
                            $image->imageable_id = $post->id;
                            $image->imageable_type = Constant::POST_IDENTIFIER;
                            $image->path = ImageService::upload($request->file('image'), Constant::IMAGES_POSTS_ROOT_PATH, $post->id, $request->file('image')->getClientOriginalName());
                            $image->save();
                        } else {
                            $image = new Image();
                            $image->imageable_id = $post->id;
                            $image->imageable_type = Constant::POST_IDENTIFIER;
                            $image->path = Constant::POST_DEFAULT_IMAGE_PATH;
                            $image->save();
                        }
                        return response()->custom(200, Constant::SUCCESS_CREATED_POST_MESSAGE, Post::with('images')->find($post->id));
                    }
                    return response()->custom(400, Constant::ERROR_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $post = Post::with('images')->find($id);
                if($post) {
                    return response()->custom(200, Constant::SUCCESS_GET_POST_MESSAGE, $post);
                }
                return response()->custom(404, Constant::POST_NOT_FOUND_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function edit(Posts $posts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin')
                {
                    $post = Post::find($id);
                    if($post) {
                        $post->fill($request->all());
                        if($request->hasFile('image')) {
                            $post_image = Image::where([['imageable_id', $post->id], ['imageable_type', Constant::POST_IDENTIFIER]])->first();
                            if($post_image && $post_image->path != Constant::POST_DEFAULT_IMAGE_PATH) {
                                ImageService::remove($post_image->path, Constant::IMAGES_POSTS_ROOT_PATH, $post->id);
                            }
                            $post_image->path = ImageService::upload($request->file('image'), Constant::IMAGES_POSTS_ROOT_PATH, $post->id, $request->file('image')->getClientOriginalName());
                            $post_image->save();
                        }
                        $post->save();
                        return response()->custom(200, Constant::SUCCESS_EDIT_MESSAGE, Post::with('images')->find($post->id));
                    }
                    return response()->custom(404, Constant::POST_NOT_FOUND_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin')
                {
                    $post = Post::find($id);
                    if($post && $post->delete()) {
                        return response()->custom(200, Constant::SUCCESS_DELETED_POST_MESSAGE, null);
                    }
                    return response()->custom(404, Constant::POST_NOT_FOUND_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function search(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $posts = Post::Search($request->keyword)->with('images')->latest()->paginate(10);
                if(count($posts)) {
                    return response()->custom(200, Constant::SUCCESS_GET_POSTS_MESSAGE, $posts);
                }
                $data['data'] = null;
                return response()->custom(200, Constant::SUCCESS_GET_POSTS_MESSAGE, $data);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }
}
