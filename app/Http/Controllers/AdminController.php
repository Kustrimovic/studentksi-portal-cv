<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Helpers\Constant;
use App\Helpers\MainModuleService;
use App\Http\Requests\Admin\CreateAdminRequest;
use App\Professor;
use App\Student;
use App\SuperAdmin;
use App\Teacher;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin') {
                    $admins = Admin::with('admineable')->latest()->paginate(10);
                    if(count($admins))
                    {
                        return response()->custom(200, Constant::SUCCESS_GET_TEACHERS_MESSAGE, $admins);
                    }
                    return response()->custom(200, Constant::SUCCESS_GET_TEACHERS_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAdminRequest $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin')
                {
                    if(array_key_exists('teacherable_id', $request->user)) {
                        if($request->user['teacherable_type'] == Constant::STUDENT_IDENTIFIER || $request->user['teacherable_type'] == "student") {
                            $student = Student::find($request->user['teacherable_id']);
                            $student->role = "admin";
                            if($student->save()) {
                                $admin = new Admin();
                                $admin->admineable_id = $student->id;
                                $admin->admineable_type = Constant::STUDENT_IDENTIFIER;
                                if($admin->save())
                                {
                                    $teacher = Teacher::find($request->user['id']);
                                    if($teacher->delete()) {
                                        return response()->custom(200, Constant::SUCCESS_ADDED_TEACHER_AS_ADMIN_MESSAGE, Admin::with('admineable')->find($admin->id));
                                    }
                                    return response()->custom(400, Constant::ERROR_MESSAGE, null);
                                }
                                return response()->custom(400, Constant::ERROR_MESSAGE, null);
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);

                        } else {
                            $professor = Professor::find($request->user['teacherable_id']);
                            $professor->role = "admin";
                            if($professor->save()) {
                                $admin = new Admin();
                                $admin->admineable_id = $professor->id;
                                $admin->admineable_type = Constant::PROFESSOR_IDENTIFIER;
                                if ($admin->save()) {
                                    $teacher = Teacher::find($request->user['id']);
                                    if($teacher->delete()) {
                                        return response()->custom(200, Constant::SUCCESS_ADDED_TEACHER_AS_ADMIN_MESSAGE, Admin::with('admineable')->find($admin->id));
                                    }
                                    return response()->custom(400, Constant::ERROR_MESSAGE, null);
                                }
                                return response()->custom(400, Constant::ERROR_MESSAGE, null);
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        }
                    } else if(array_key_exists('role', $request->user)) {
                        $student = Student::find($request->user['id']);
                        $student->role = "admin";
                        if($student->save()) {
                            $admin = new Admin();
                            $admin->admineable_id = $student->id;
                            $admin->admineable_type = Constant::STUDENT_IDENTIFIER;
                            if($admin->save())
                            {
                                return response()->custom(200, Constant::SUCCESS_ADDED_STUDENT_AS_ADMIN_MESSAGE, Admin::with('admineable')->find($admin->id));
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        }
                        return response()->custom(400, Constant::ERROR_MESSAGE, null);

                    } else {
                        $professor = Professor::where('user_id', $request->user['id'])->first();
                        if(!$professor)
                        {
                            $professor = new Professor();
                            $professor->user_id = $request->user['id'];
                            $professor->first_name = $request->user['name'];
                            $professor->last_name = $request->user['lastname'];
                            $professor->role = "admin";
                            if($professor->save())
                            {
                                $admin = new Admin();
                                $admin->admineable_id = $professor->id;
                                $admin->admineable_type = Constant::PROFESSOR_IDENTIFIER;
                                if($admin->save())
                                {
                                    return response()->custom(200, Constant::SUCCESS_ADDED_PROFESSOR_AS_ADMIN_MESSAGE, Admin::with('admineable')->find($admin->id));
                                }
                                return response()->custom(400, Constant::ERROR_MESSAGE, null);
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        }
                        $professor->role = "admin";
                        $professor->save();
                        $admin = new Admin();
                        $admin->admineable_id = $professor->id;
                        $admin->admineable_type = Constant::PROFESSOR_IDENTIFIER;
                        if($admin->save())
                        {
                            return response()->custom(200, Constant::SUCCESS_ADDED_PROFESSOR_AS_ADMIN_MESSAGE, Admin::with('admineable')->find($admin->id));
                        }
                        return response()->custom(400, Constant::ERROR_MESSAGE, null);
                    }
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin')
                {
                    $admin = Admin::find($id);
                    if($admin->admineable_type == Constant::STUDENT_IDENTIFIER) {
                        $student = Student::find($admin->admineable_id);
                        $student->role = 'student';
                        $student->save();
                    } else if($admin->admineable_type == Constant::PROFESSOR_IDENTIFIER) {
                        $professor = Professor::find($admin->admineable_id);
                        $professor->role = 'professor';
                        $professor->save();
                    } else {
                        $superAdmin = SuperAdmin::find($admin->admineable_id);
                        $superAdmin->delete();
                    }

                    if($admin && $admin->delete())
                    {
                        return response()->custom(200, Constant::SUCCESS_REMOVED_ADMIN_ROLE_MESSAGE, null);
                    }
                    return response()->custom(404, Constant::ADMIN_NOT_FOUND_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }
}
