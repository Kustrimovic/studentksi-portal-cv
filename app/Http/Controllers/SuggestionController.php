<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Company;
use App\Helpers\Constant;
use App\Helpers\MainModuleService;
use App\Http\Requests\Suggestion\CreateSuggestionRequest;
use App\Http\Requests\Suggestion\UpdateSuggestionRequest;
use App\Student;
use App\Suggestion;
use App\Teacher;
use Illuminate\Http\Request;

class SuggestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                switch ($response['body']['entity']['role'])
                {
                    case 'admin':
                        $suggestions = Suggestion::with('student')->latest()->paginate(10);
                        foreach ($suggestions as $suggestion)
                        {
                            if($suggestion->suggestionable_type == Constant::TEACHER_IDENTIFIER) {
                                $teacher = Teacher::where('id', $suggestion->suggestionable_id)->first();
                                $suggestion->creator = $teacher->teacherable;
                                $suggestion->canEdit = false;
                            } else if($suggestion->suggestionable_type == Constant::ADMIN_IDENTIFIER) {
                                $admin = Admin::where('id', $suggestion->suggestionable_id)->first();
                                $suggestion->creator = $admin->admineable;
                                $suggestion->canEdit = false;
                                if($admin->admineable_type == $response['body']['entity']['model'] && $admin->admineable_id == $response['body']['entity']['id']) {
                                    $suggestion->canEdit = true;
                                }
                            } else if($suggestion->suggestionable_type == Constant::COMPANY_IDENTIFIER) {
                                $company = Company::where('id', $suggestion->suggestionable_id)->first();
                                $suggestion->creator = $company;
                                $suggestion->canEdit = false;
                            }
                        }
                        break;
                    case 'teacher':
                        $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                        $suggestions = Suggestion::with('student')->where([['suggestionable_type', Constant::TEACHER_IDENTIFIER], ['suggestionable_id', $teacher->id]])->latest()->paginate(10);
                        foreach ($suggestions as $suggestion) {
                            $suggestion->canEdit = true;
                        }
                        break;
                    case 'company':
                        $suggestions = Suggestion::with('student')->where([['suggestionable_type', Constant::COMPANY_IDENTIFIER], ['suggestionable_id', $response['body']['entity']['id']]])->latest()->paginate(10);
                        foreach ($suggestions as $suggestion) {
                            $suggestion->canEdit = true;
                        }
                        break;
                    default:
                        return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                }
                if(count($suggestions)) {
                    return response()->custom(200, Constant::SUCCESS_GET_SUGGESTIONS_MESSAGE, $suggestions);
                }
                return response()->custom(200, Constant::SUCCESS_GET_SUGGESTIONS_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSuggestionRequest $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $suggestion = new Suggestion();
                $suggestion->fill($request->all());
                $suggestion->student_id = $request->get('student_id');
                switch ($response['body']['entity']['role'])
                {
                    case 'admin':
                        $admin = Admin::where([['admineable_type', $response['body']['entity']['model']], ['admineable_id', $response['body']['entity']['id']]])->first();
                        $suggestion->suggestionable_id = $admin->id;
                        $suggestion->suggestionable_type = Constant::ADMIN_IDENTIFIER;
                        break;
                    case 'teacher':
                        $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                        $suggestion->suggestionable_id = $teacher->id;
                        $suggestion->suggestionable_type = Constant::TEACHER_IDENTIFIER;
                        break;
                    case 'company':
                        $suggestion->suggestionable_id = $response['body']['entity']['id'];
                        $suggestion->suggestionable_type = Constant::COMPANY_IDENTIFIER;
                        break;
                    default:
                        return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                }
                if($suggestion->save())
                {
                    return response()->custom(200, Constant::SUCCESS_CREATED_SUGGESTION_MESSAGE, Suggestion::with('student')->find($suggestion->id));
                }
                return response()->custom(400, Constant::ERROR_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Suggestion $suggestion
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Suggestion $suggestion
     * @return \Illuminate\Http\Response
     */
    public function edit(Suggestion $ad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Suggestion $suggestion
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSuggestionRequest $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                switch ($response['body']['entity']['role'])
                {
                    case 'admin':
                        $suggestion = Suggestion::find($id);
                        if($suggestion)
                        {
                            $admin = Admin::where([['admineable_type', $response['body']['entity']['model']], ['admineable_id', $response['body']['entity']['id']]])->first();
                            if($admin && $suggestion->suggestionable_type == Constant::ADMIN_IDENTIFIER && $suggestion->suggestionable_id == $admin->id)
                            {
                                $suggestion->fill($request->all());
                                if($suggestion->save())
                                {
                                    return response()->custom(200, Constant::SUCCESS_EDIT_MESSAGE, Suggestion::with('student')->find($suggestion->id));
                                }
                                return response()->custom(400, Constant::ERROR_MESSAGE, null);
                            }
                            return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                        }
                        return response()->custom(404, Constant::SUGGESTION_NOT_FOUND_MESSAGE, null);
                        break;
                    case 'teacher':
                        $suggestion = Suggestion::find($id);

                        if($suggestion)
                        {
                            $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                            if($teacher)
                            {
                                if($suggestion->suggestionable_type == Constant::TEACHER_IDENTIFIER && $suggestion->suggestionable_id == $teacher->id)
                                {
                                    $suggestion->fill($request->all());
                                    if($suggestion->save())
                                    {
                                        return response()->custom(200, Constant::SUCCESS_EDIT_MESSAGE, Suggestion::with('student')->find($suggestion->id));
                                    }
                                    return response()->custom(400, Constant::ERROR_MESSAGE, null);
                                }
                                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);

                            }
                            return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                        }
                        return response()->custom(404, Constant::SUGGESTION_NOT_FOUND_MESSAGE, null);
                        break;
                    case 'company':
                        $suggestion = Suggestion::find($id);
                        if($suggestion)
                        {
                            if($suggestion->suggestionable_type == Constant::COMPANY_IDENTIFIER && $suggestion->suggestionable_id == $response['body']['entity']['id'])
                            {
                                $suggestion->fill($request->all());
                                if($suggestion->save())
                                {
                                    return response()->custom(200, Constant::SUCCESS_EDIT_MESSAGE, Suggestion::with('student')->find($suggestion->id));
                                }
                                return response()->custom(400, Constant::ERROR_MESSAGE, null);
                            }
                            return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                        }
                        return response()->custom(404, Constant::SUGGESTION_NOT_FOUND_MESSAGE, null);
                        break;
                    default:
                        return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                }
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Suggestion $suggestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                switch ($response['body']['entity']['role'])
                {
                    case 'admin':
                        if(Suggestion::destroy($id))
                        {
                            return response()->custom(200, Constant::SUCCESS_DELETED_SUGGESTION_MESSAGE, null);
                        }
                        return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        break;
                    case 'teacher':
                        $suggestion = Suggestion::find($id);

                        if($suggestion)
                        {
                            $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                            if($teacher)
                            {
                                if($suggestion->suggestionable_type == Constant::TEACHER_IDENTIFIER && $suggestion->suggestionable_id == $teacher->id)
                                {
                                    if(Suggestion::destroy($id))
                                    {
                                        return response()->custom(200, Constant::SUCCESS_DELETED_SUGGESTION_MESSAGE, null);
                                    }
                                    return response()->custom(400, Constant::ERROR_MESSAGE, null);
                                }
                                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);

                            }
                            return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                        }
                        return response()->custom(404, Constant::SUGGESTION_NOT_FOUND_MESSAGE, null);
                        break;
                    case 'company':
                        $suggestion = Suggestion::find($id);
                        if($suggestion)
                        {
                            if($suggestion->suggestionable_type == Constant::COMPANY_IDENTIFIER && $suggestion->suggestionable_id == $response['body']['entity']['id'])
                            {
                                if(Suggestion::destroy($id))
                                {
                                    return response()->custom(200, Constant::SUCCESS_DELETED_SUGGESTION_MESSAGE, null);
                                }
                                return response()->custom(400, Constant::ERROR_MESSAGE, null);
                            }
                            return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                        }
                        return response()->custom(404, Constant::SUGGESTION_NOT_FOUND_MESSAGE, null);
                        break;
                    default:
                        return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                }
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }
}
