<?php

namespace App\Http\Controllers;

use App\Company;
use App\Helpers\Constant;
use App\Helpers\MainModuleService;
use App\Http\Requests\Auth\AuthRequest;
use App\Student;
use App\User;

class UserController extends Controller
{
    public function authorization(AuthRequest $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }
}
