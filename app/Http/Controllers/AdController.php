<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Applicant;
use App\Company;
use App\Contact;
use App\Helpers\Constant;
use App\Helpers\MainModuleService;
use App\Http\Requests\Ad\CreateAdRequest;
use App\Http\Requests\Ad\UpdateAdRequest;
use App\Student;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'student' || ($response['body']['entity']['role'] == 'teacher' && $response['body']['entity']['model'] == Constant::STUDENT_IDENTIFIER))
                {
                    if($response['body']['entity']['role'] == 'student')
                    {
                        $ads = Ad::with('company.logo')->whereDoesntHave('applicants', function ($q) use ($response) {
                            $q->where('student_id', $response['body']['entity']['id']);
                        })->latest()->paginate(10);
                    } else {
                        $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                        $ads = Ad::with('company.logo')->whereDoesntHave('applicants', function ($q) use ($response, $teacher) {
                            $q->where('student_id', $teacher->teacherable_id);
                        })->latest()->paginate(10);
                    }
                } else {
                    $ads = Ad::with('company.logo')->latest()->paginate(10);
                }
                if(count($ads)) {
                    return response()->custom(200, Constant::SUCCESS_GET_ADS_MESSAGE, $ads);
                }
                return response()->custom(200, Constant::SUCCESS_GET_ADS_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAdRequest $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'company') {
                    $ad = new Ad();
                    $ad->fill($request->all());
                    $ad->company_id = $response['body']['entity']['id'];
                    if($ad->save()) {
                        return response()->custom(200, Constant::SUCCESS_CREATED_AD_MESSAGE, Ad::find($ad->id));
                    }
                    return response()->custom(400, Constant::ERROR_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'company')
                {
                    $ad = Ad::with('company.logo', 'applicants.image', 'applicants.certificates')->find($id);
                    $ad->canApply = false;
                } else if($response['body']['entity']['role'] == 'student' || ($response['body']['entity']['role'] == 'teacher' && $response['body']['entity']['model'] == Constant::STUDENT_IDENTIFIER)) {
                    $ad = Ad::with('company.logo')->find($id);
                    if($response['body']['entity']['role'] == 'student')
                    {
                        $applicant = Applicant::where([['student_id', $response['body']['entity']['id']], ['ad_id', $ad->id]])->first();
                    } else {
                        $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                        $applicant = Applicant::where([['student_id', $teacher->teacherable_id], ['ad_id', $ad->id]])->first();
                    }
                    if($applicant) {
                        $ad->canApply = false;
                    } else {
                        $ad->canApply = true;
                    }
                } else {
                    $ad = Ad::with('company.logo')->find($id);
                    $ad->canApply = false;
                }
                if($ad) {
                    return response()->custom(200, Constant::SUCCESS_GET_AD_MESSAGE, $ad);
                }
                return response()->custom(404, Constant::AD_NOT_FOUND, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdRequest $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'company')
                {
                    $ad = Ad::find($id);
                    if($ad)
                    {
                        if($ad->company_id == $response['body']['entity']['id'])
                        {
                            $ad->fill($request->all());
                            $ad->save();
                            return response()->custom(200, Constant::SUCCESS_EDIT_MESSAGE, Ad::with('company.logo')->find($ad->id));
                        }
                        return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                    }
                    return response()->custom(404, Constant::AD_NOT_FOUND, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'company')
                {
                    $ad = Ad::find($id);
                    if($ad)
                    {
                        if($ad->company_id == $response['body']['entity']['id'])
                        {
                            if($ad->delete())
                            {
                                return response()->custom(200, Constant::SUCCESS_DELETE_MESSAGE, null);
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        }
                        return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                    }
                    return response()->custom(404, Constant::AD_NOT_FOUND, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function companyAds(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'company')
                {
                    $ads = Ad::with('company.logo', 'applicants.image', 'applicants.certificates')->where('company_id', $response['body']['entity']['id'])->get();
                    if(count($ads))
                    {
                        return response()->custom(200, Constant::SUCCESS_GET_ADS_MESSAGE, $ads);
                    }
                    return response()->custom(200, Constant::SUCCESS_GET_ADS_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function studentAdsApplications(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'student' || ($response['body']['entity']['role'] == 'teacher' && $response['body']['entity']['model'] == Constant::STUDENT_IDENTIFIER))
                {
                    if($response['body']['entity']['role'] == 'student')
                    {
                        $ads = Ad::with('company.logo')->whereHas('applicants', function ($q) use ($response) {
                            $q->where('student_id', $response['body']['entity']['id']);
                        })->latest()->paginate(10);
                    } else {
                        $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                        $ads = Ad::with('company.logo')->whereHas('applicants', function ($q) use ($response, $teacher) {
                            $q->where('student_id', $teacher->teacherable_id);
                        })->latest()->paginate(10);
                    }
                    if(count($ads))
                    {
                        return response()->custom(200, Constant::SUCCESS_GET_ADS_MESSAGE, $ads);
                    }
                    $data['data']['length'] = 0;
                    return response()->custom(200, Constant::SUCCESS_GET_ADS_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function search(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($request->has('category') && $request->category != 'all')
                {
                    $ads = Ad::Search($request->keyword)->where('category', $request->category)->with('company.logo')->latest()->paginate(10);
                } else {
                    $ads = Ad::Search($request->keyword)->with('company.logo')->latest()->paginate(10);
                }
                if(count($ads)) {
                    return response()->custom(200, Constant::SUCCESS_GET_ADS_MESSAGE, $ads);
                }
                $data['data'] = null;
                return response()->custom(200, Constant::SUCCESS_GET_ADS_MESSAGE, $data);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function applyAd(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'student' || ($response['body']['entity']['role'] == 'teacher' && $response['body']['entity']['model'] == Constant::STUDENT_IDENTIFIER))
                {
                    if($response['body']['entity']['role'] == 'student') {
                        $application = Applicant::where([['student_id', $response['body']['entity']['id']], ['ad_id', $id]])->first();
                    } else {
                        $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                        $application = Applicant::where([['student_id', $teacher->teacherable_id], ['ad_id', $id]])->first();
                    }

                    if(!$application)
                    {
                        $ad = Ad::where('id', $id)->first();
                        if($response['body']['entity']['role'] == 'student') {
                            $ad->applicants()->attach($response['body']['entity']['id']);
                        } else {
                            $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                            $ad->applicants()->attach($teacher->teacherable_id);
                        }

                        $contactMail = Contact::first();
                        $company = Company::where('id', $ad->company_id)->first();
                        Mail::send([], [], function ($message) use ($company, $contactMail, $response) {
                            $message->to($company->email)
                                ->subject("Aplikacija za oglas")
                                ->from($contactMail->email)
                                ->setBody("Poštovani, za vaš oglas aplicirao je student "
                                    .$response['body']['entity']['first_name']." "
                                    .$response['body']['entity']['last_name'].". Studentov CV možete pogledati na linku: "
                                    .Constant::STUDENT_SINGLE_VIEW_ROUTE.$response['body']['entity']['id']);
                        });
                        return response()->custom(200, Constant::SUCCESS_APPLIED_MESSAGE, $ad);
                    }
                    return response()->custom(403, Constant::ALREADY_APPLIED_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }
}
