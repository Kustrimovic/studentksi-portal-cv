<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\MainModuleService;
use App\Http\Requests\Teacher\CreateTeacherRequest;
use App\Professor;
use App\Student;
use App\Teacher;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $teachers = Teacher::with('teacherable')->latest()->paginate(10);
                if(count($teachers))
                {
                    return response()->custom(200, Constant::SUCCESS_GET_TEACHERS_MESSAGE, $teachers);
                }
                return response()->custom(200, Constant::SUCCESS_GET_TEACHERS_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTeacherRequest $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin')
                {
                    if($request->user['role'] == 'student') {
                        $student = Student::find($request->user['id']);
                        $student->role = "teacher";
                        if($student->save())
                        {
                            $teacher = new Teacher();
                            $teacher->teacherable_id = $student->id;
                            $teacher->teacherable_type = Constant::STUDENT_IDENTIFIER;
                            if($teacher->save())
                            {
                                return response()->custom(200, Constant::SUCCESS_ADDED_STUDENT_AS_TEACHER_MESSAGE, Teacher::with('teacherable')->find($teacher->id));
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        }
                        return response()->custom(400, Constant::ERROR_MESSAGE, null);
                    }
                    else {
                        $professor = Professor::where('user_id', $request->user['id'])->first();
                        if(!$professor)
                        {
                            $professor = new Professor();
                            $professor->user_id = $request->user['id'];
                            $professor->first_name = $request->user['name'];
                            $professor->last_name = $request->user['lastname'];
                            $professor->role = "teacher";
                            if($professor->save())
                            {
                                $teacher = new Teacher();
                                $teacher->teacherable_id = $professor->id;
                                $teacher->teacherable_type = Constant::PROFESSOR_IDENTIFIER;
                                if($teacher->save())
                                {
                                    return response()->custom(200, Constant::SUCCESS_ADDED_PROFESSOR_AS_TEACHER_MESSAGE, Teacher::with('teacherable')->find($teacher->id));
                                }
                                return response()->custom(400, Constant::ERROR_MESSAGE, null);
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        }
                        $professor->role = "teacher";
                        $professor->save();
                        $teacher = new Teacher();
                        $teacher->teacherable_id = $professor->id;
                        $teacher->teacherable_type = Constant::PROFESSOR_IDENTIFIER;
                        if($teacher->save())
                        {
                            return response()->custom(200, Constant::SUCCESS_ADDED_PROFESSOR_AS_TEACHER_MESSAGE, Teacher::with('teacherable')->find($teacher->id));
                        }
                        return response()->custom(400, Constant::ERROR_MESSAGE, null);
                    }
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teacher $teacher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin')
                {
                    $teacher = Teacher::find($id);
                    if($teacher->teacherable_type == Constant::STUDENT_IDENTIFIER) {
                        $student = Student::find($teacher->teacherable_id);
                        $student->role = 'student';
                        $student->save();
                    } else {
                        $professor = Professor::find($teacher->teacherable_id);
                        $professor->role = 'professor';
                        $professor->save();
                    }
                    if($teacher && $teacher->delete())
                    {
                        return response()->custom(200, Constant::SUCCESS_REMOVED_TEACHER_ROLE_MESSAGE, null);
                    }
                    return response()->custom(404, Constant::TEACHER_NOT_FOUND_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function allTeachers(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $teachers = Teacher::with('teacherable')->latest()->get();
                if(count($teachers))
                {
                    return response()->custom(200, Constant::SUCCESS_GET_TEACHERS_MESSAGE, $teachers);
                }
                return response()->custom(200, Constant::SUCCESS_GET_TEACHERS_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }
}
