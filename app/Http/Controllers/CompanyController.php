<?php

namespace App\Http\Controllers;

use App\Company;
use App\Helpers\Constant;
use App\Helpers\ImageService;
use App\Helpers\MainModuleService;
use App\Http\Requests\Company\CreateCompanyRequest;
use App\Http\Requests\Company\UpdateCompanyRequest;
use App\Image;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $companies = Company::with('ads', 'logo')->latest()->paginate(10);
                if(count($companies)) {
                    return response()->custom(200, Constant::SUCCESS_GET_COMPANIES_MESSAGE, $companies);
                }
                return response()->custom(200, Constant::SUCCESS_GET_COMPANIES_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCompanyRequest $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin')
                {
                    $companyResponse = MainModuleService::storeCompany($request->all());
                    if(!$companyResponse['error'])
                    {
                        $company = new Company();
                        $company->fill($request->except('password'));
                        $company->company_id = $companyResponse['body']['entity']['id'];
                        if($company->save())
                        {
                            if($request->hasFile('logo')) {
                                $image = new Image();
                                $image->imageable_id = $company->id;
                                $image->imageable_type = Constant::COMPANY_IDENTIFIER;
                                $image->path = ImageService::upload($request->file('logo'), Constant::IMAGES_COMPANIES_ROOT_PATH, $company->id, $request->file('logo')->getClientOriginalName());
                                $image->save();
                            } else {
                                $image = new Image();
                                $image->imageable_id = $company->id;
                                $image->imageable_type = Constant::COMPANY_IDENTIFIER;
                                $image->path = Constant::COMPANY_DEFAULT_IMAGE_PATH;
                                $image->save();
                            }
                            return response()->custom(200, Constant::SUCCESS_CREATED_COMPANY_MESSAGE, Company::with('logo')->where('id', $company->id)->first());
                        }
                        return response()->custom(400, Constant::ERROR_MESSAGE, null);
                    }
                    return response()->custom($companyResponse['body']['status'], $companyResponse['body']['message'], $companyResponse['body']['entity']);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $company = Company::with('ads', 'logo')->find($id);
                if($company)
                {
                    return response()->custom(200, Constant::SUCCESS_GET_COMPANY_MESSAGE, $company);
                }
                return response()->custom(404, 'Company not found', null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin')
                {
                    $company = Company::find($id);
                    if($company)
                    {
                        $companyResponse = MainModuleService::updateCompany($request->all(), $company->company_id);
                        if(!$companyResponse['error'])
                        {
                            $company->fill($request->except('password'));
                            if($company->save())
                            {
                                if($request->hasFile('logo')) {
                                    $company_image = Image::where([['imageable_id', $company->id], ['imageable_type', Constant::COMPANY_IDENTIFIER]])->first();
                                    if($company_image && $company_image->path != Constant::COMPANY_DEFAULT_IMAGE_PATH) {
                                        ImageService::remove($company_image->path, Constant::IMAGES_COMPANIES_ROOT_PATH, $company->id);
                                    }
                                    $company_image->path = ImageService::upload($request->file('logo'), Constant::IMAGES_COMPANIES_ROOT_PATH, $company->id, $request->file('logo')->getClientOriginalName());
                                    $company_image->save();
                                }
                                return response()->custom(200, Constant::SUCCESS_EDIT_MESSAGE, Company::with('ads', 'logo')->find($id));
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        }
                        return response()->custom($companyResponse['body']['status'], $companyResponse['body']['message'], $companyResponse['body']['entity']);
                    }
                    return response()->custom(404, Constant::COMPANY_NOT_FOUND_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'admin')
                {
                    $company = Company::find($id);
                    if($company)
                    {
                        $companyResponse = MainModuleService::destroyCompany($request['token'], $company->company_id);
                        if(!$companyResponse['error'])
                        {
                            if($company->destroy($id))
                            {
                                return response()->custom(200, Constant::SUCCESS_DELETE_MESSAGE, null);
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        }
                        return response()->custom($companyResponse['body']['status'], $companyResponse['body']['message'], $companyResponse['body']['entity']);
                    }
                    return response()->custom(404, Constant::COMPANY_NOT_FOUND_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function search(Request $request)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $companies = Company::Search($request->keyword)->with('ads', 'logo')->latest()->paginate(10);
                if(count($companies)) {
                    return response()->custom(200, Constant::SUCCESS_GET_COMPANIES_MESSAGE, $companies);
                }
                $data['data'] = null;
                return response()->custom(200, Constant::SUCCESS_GET_COMPANIES_MESSAGE, $data);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }
}
