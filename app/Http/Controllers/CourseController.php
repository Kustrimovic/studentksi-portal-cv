<?php

namespace App\Http\Controllers;

use App\Certificate;
use App\Contact;
use App\Course;
use App\CourseApplicant;
use App\Helpers\Constant;
use App\Helpers\ImageService;
use App\Helpers\MainModuleService;
use App\Http\Requests\Course\CreateCourseRequest;
use App\Image;
use App\Professor;
use App\Student;
use App\StudentCourse;
use App\Teacher;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('token')) {
            $response = MainModuleService::auth($request->get('token'), false);
            if (!$response['error']) {
                switch ($response['body']['entity']['role'])
                {
                    case "admin":
                        $courses = Course::with('image', 'teacher.teacherable', 'students', 'certificate')->latest()->paginate(10);
                        foreach ($courses as $course)
                        {
                            $course->canEdit = true;
                        }
                        break;
                    case "teacher":
                        $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                        $courses = Course::with('image', 'teacher.teacherable', 'students', 'certificate')->where([['teacher_id', '!=', $teacher->id], ['status', '<', 3]])->latest()->paginate(10);
                        foreach ($courses as $course)
                        {
                            $course->canEdit = false;
                        }
                        break;
                    default:
                        $courses = Course::with('image', 'teacher.teacherable', 'students', 'certificate')->where('status', '<', 3)->latest()->paginate(10);
                        foreach ($courses as $course)
                        {
                            $course->canEdit = false;
                        }
                }
                if (count($courses)) {
                    return response()->custom(200, Constant::SUCCESS_GET_COURSES_MESSAGE, $courses);
                }
                return response()->custom(200, Constant::SUCCESS_GET_COURSES_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCourseRequest $request)
    {
        if ($request->has('token')) {
            $response = MainModuleService::auth($request->get('token'), false);
            if (!$response['error']) {
                if ($response['body']['entity']['role'] == 'admin') {
                    $course = new Course();
                    $course->fill($request->all());
                    $course->teacher_id = $request->get('teacher_id');
                    if (!$request->has('status')) {
                        $course->status = 1;
                    }
                    $course->certificate_id = $request->get('certificate_id');
                    if ($course->save()) {
                        if ($request->hasFile('logo')) {
                            $path = ImageService::upload($request->file('logo'), Constant::IMAGES_COURSES_ROOT_PATH, $course->id, $request->file('logo')->getClientOriginalName());
                            $image = new Image();
                            $image->imageable_id = $course->id;
                            $image->imageable_type = Constant::COURSE_IDENTIFIER;
                            $image->path = $path;
                            $image->created_at = Carbon::now();
                            $image->save();
                        } else {
                            $image = new Image();
                            $image->imageable_id = $course->id;
                            $image->imageable_type = Constant::COURSE_IDENTIFIER;
                            $image->path = Constant::COURSES_DEFAULT_IMAGE_PATH;
                            $image->created_at = Carbon::now();
                            $image->save();
                        }
                        if ($request->has('students')) {
                            $course->students()->attach(explode(',', $request->students));
                        }
                        return response()->custom(200, Constant::SUCCESS_CREATED_COURSE_MESSAGE, Course::with('image', 'teacher.teacherable', 'students', 'certificate')->find($course->id));
                    }
                    return response()->custom(400, Constant::ERROR_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course $course
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->has('token')) {
            $response = MainModuleService::auth($request->get('token'), false);
            if (!$response['error']) {
                $course = $course = Course::find($id);
                if ($course) {
                    switch ($response['body']['entity']['role']) {
                        case 'company':
                            $course = Course::with('image', 'teacher.teacherable', 'students', 'certificate')->find($id);
                            $course->canApply = false;
                            $course->canApprove = false;
                            break;
                        case 'student':
                            $course = Course::with('image', 'teacher.teacherable', 'students', 'certificate')->find($id);
                            $course->canApprove = false;
                            $applicant = CourseApplicant::where([['student_id', $response['body']['entity']['id']], ['course_id', $course->id]])->first();
                            if ($applicant) {
                                $course->canApply = false;
                            } else {
                                $student = StudentCourse::where([['student_id', $response['body']['entity']['id']], ['course_id', $course->id]])->first();
                                if($student) {
                                    $course->canApply = false;
                                } else {
                                    $course->canApply = true;
                                }
                            }
                            break;
                        case 'teacher':
                            $course = Course::with('image', 'teacher.teacherable', 'students', 'certificate', 'applicants')->find($id);
                            $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                            if ($teacher->id != $course->teacher_id) {
                                $course->canApprove = false;
                                if ($response['body']['entity']['model'] == Constant::STUDENT_IDENTIFIER) {
                                    $applicant = CourseApplicant::where([['student_id', $teacher->teacherable_id], ['course_id', $course->id]])->first();
                                    if ($applicant) {
                                        $course->canApply = false;
                                    } else {
                                        $student = StudentCourse::where([['student_id', $teacher->teacherable_id], ['course_id', $course->id]])->first();
                                        if($student) {
                                            $course->canApply = false;
                                        } else {
                                            $course->canApply = true;
                                        }
                                    }
                                } else {
                                    $course->canApply = false;
                                }
                            } else {
                                $course->canApprove = true;
                                $course->canApply = false;
                            }
                            break;
                        case 'admin':
                            $course = Course::with('image', 'teacher.teacherable', 'students', 'certificate', 'applicants')->find($id);
                            $course->canApprove = true;
                            $course->canApply = false;
                            break;
                    }

                    return response()->custom(200, Constant::SUCCESS_GET_COURSE_MESSAGE, $course);
                }
                return response()->custom(404, 'Course not found', null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Course $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->has('token')) {
            $response = MainModuleService::auth($request->get('token'), false);
            if (!$response['error']) {
                switch ($response['body']['entity']['role']) {
                    case 'admin':
                        $course = Course::find($id);
                        if ($course) {
                            if($request->name != "null") {
                                $course->name = $request->name;
                            }
                            if($request->description != "null") {
                                $course->description = $request->description;
                            }
                            if($request->from != "null") {
                                $course->from = $request->from;
                            }
                            if($request->to != "null") {
                                $course->to = $request->to;
                            }
                            if($request->status != "null") {
                                $course->status = $request->status;
                            }
                            if ($request->hasFile('logo')) {
                                $course_logo = Image::where([['imageable_id', $course->id], ['imageable_type', Constant::COURSE_IDENTIFIER]])->first();
                                if ($course_logo && $course_logo->path != Constant::COURSES_DEFAULT_IMAGE_PATH) {
                                    ImageService::remove($course_logo->path, Constant::IMAGES_COURSES_ROOT_PATH, $course->id);
                                }
                                $course_logo->path = ImageService::upload($request->file('logo'), Constant::IMAGES_COURSES_ROOT_PATH, $course->id, $request->file('logo')->getClientOriginalName());;
                                $course_logo->save();
                            }
                            if ($request->has('students')) {
                                $course->students()->detach(explode(',', $request->students));
                                $course->students()->attach(explode(',', $request->students));
                            }
                            if ($request->has('teacher_id')) {
                                $course->teacher_id = $request->get('teacher_id');
                            }
                            if ($course->save()) {
                                return response()->custom(200, Constant::SUCCESS_EDIT_MESSAGE, Course::with('image', 'teacher.teacherable', 'students', 'certificate')->find($course->id));
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        }
                        return response()->custom(404, 'Course not found', null);
                    case 'teacher':
                        $course = Course::find($id);
                        if ($course) {
                            $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                            if ($teacher && $teacher->id == $course->teacher_id) {
                                $course->fill($request->all());
                                if ($request->hasFile('logo')) {
                                    $course_logo = Image::where([['imageable_id', $course->id], ['imageable_type', Constant::COURSE_IDENTIFIER]])->first();
                                    if ($course_logo) {
                                        ImageService::remove($course_logo->path, Constant::IMAGES_COURSES_ROOT_PATH, $course->id);
                                    }
                                    $course_logo->path = ImageService::upload($request->file('logo'), Constant::IMAGES_COURSES_ROOT_PATH, $course->id, $request->file('logo')->getClientOriginalName());;
                                    $course_logo->save();
                                }
                                if ($request->has('students')) {
                                    $course->students()->attach(explode(',', $request->students));
                                }
                                if ($request->has('teacher_id')) {
                                    $course->teacher_id = $request->get('teacher_id');
                                }
                                if ($course->save()) {
                                    return response()->custom(200, Constant::SUCCESS_EDIT_MESSAGE, Course::with('image', 'teacher.teacherable', 'students', 'certificate')->find($course->id));
                                }
                                return response()->custom(400, Constant::ERROR_MESSAGE, null);
                            }
                            return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                        }
                        return response()->custom(404, 'Course not found', null);
                    default:
                        return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                }
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->has('token')) {
            $response = MainModuleService::auth($request->get('token'), false);
            if (!$response['error']) {
                switch ($response['body']['entity']['role']) {
                    case 'admin':
                        $course = Course::find($id);
                        if ($course) {
                            if ($course->delete()) {
                                return response()->custom(200, Constant::SUCCESS_DELETE_COURSE_MESSAGE, null);
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        }
                        return response()->custom(404, Constant::COURSE_NOT_FOUND_MESSAGE, null);
                    case 'teacher':
                        $course = Course::find($id);
                        if ($course) {
                            $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                            if ($teacher && $teacher->id == $course->teacher_id) {
                                if ($course->delete()) {
                                    return response()->custom(200, Constant::SUCCESS_DELETE_COURSE_MESSAGE, null);
                                }
                                return response()->custom(400, Constant::ERROR_MESSAGE, null);
                            }
                            return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                        }
                        return response()->custom(404, Constant::COURSE_NOT_FOUND_MESSAGE, null);
                    default:
                        return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                }
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function complete(Request $request, $id)
    {
        if ($request->has('token')) {
            $response = MainModuleService::auth($request->get('token'), false);
            if (!$response['error']) {
                switch ($response['body']['entity']['role']) {
                    case 'admin':
                        $course = Course::find($id);
                        if ($course) {
                            if ($course->status != 3) {
                                $course->status = 3;
                                if ($course->save()) {
                                    if ($request->has('students')) {
                                        $certificate = Certificate::find($course->certificate_id);
                                        if ($certificate) {
                                            foreach ($request->students as $student) {
                                                $certificate->students()->attach($student['id'], ['rating' => $student['rating']]);
                                            }
                                            return response()->custom(200, Constant::SUCCESS_COMPLETED_COURSE_MESSAGE, Course::with('image', 'students', 'certificate.students')->find($course->id));
                                        }
                                        return response()->custom(404, Constant::CERTIFICATE_FOR_COURSE_NOT_FOUND, null);
                                    }
                                    return response()->custom(200, Constant::SUCCESS_COMPLETED_COURSE_MESSAGE, Course::with('image', 'students', 'certificate.students')->find($course->id));
                                }
                                return response()->custom(400, Constant::ERROR_MESSAGE, null);
                            }
                            return response()->custom(200, Constant::ALREADY_COMPLETED_COURSE_MESSAGE, Course::with('image', 'students', 'certificate')->find($course->id));
                        }
                        return response()->custom(404, Constant::COURSE_NOT_FOUND_MESSAGE, null);
                    case 'teacher':
                        $course = Course::find($id);
                        if ($course) {
                            $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                            if ($teacher && $teacher->id == $course->teacher_id) {
                                if ($course->status != 3) {
                                    $course->status = 3;
                                    if ($course->save()) {
                                        if ($request->has('students')) {
                                            $certificate = Certificate::find($course->certificate_id);
                                            if ($certificate) {
                                                $certificate->students()->attach($request->get('students'));
                                                return response()->custom(200, Constant::SUCCESS_COMPLETED_COURSE_MESSAGE, Course::with('image', 'students', 'certificate')->find($course->id));
                                            }
                                            return response()->custom(404, Constant::CERTIFICATE_FOR_COURSE_NOT_FOUND, null);
                                        }
                                        return response()->custom(200, Constant::SUCCESS_COMPLETED_COURSE_MESSAGE, Course::with('image', 'students', 'certificate')->find($course->id));
                                    }
                                    return response()->custom(400, Constant::ERROR_MESSAGE, null);
                                }
                                return response()->custom(200, Constant::ALREADY_COMPLETED_COURSE_MESSAGE, Course::with('image', 'students', 'certificate')->find($course->id));
                            }
                            return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                        }
                        return response()->custom(404, Constant::COURSE_NOT_FOUND_MESSAGE, null);
                    default:
                        return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                }
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function myCourses(Request $request)
    {
        if ($request->has('token')) {
            $response = MainModuleService::auth($request->get('token'), false);
            if (!$response['error']) {
                if ($response['body']['entity']['role'] == 'teacher') {
                    $teacher = Teacher::where([['teacherable_id', $response['body']['entity']['id']], ['teacherable_type', $response['body']['entity']['model']]])->first();
                    if ($teacher) {
                        $courses = Course::with('image', 'certificate', 'students', 'applicants')->where('teacher_id', $teacher->id)->latest()->paginate(10);
                        foreach ($courses as $course)
                        {
                            $course->canEdit = true;
                        }
                        if (count($courses)) {
                            return response()->custom(200, Constant::SUCCESS_GET_COURSES_MESSAGE, $courses);
                        }
                        return response()->custom(200, Constant::SUCCESS_GET_COURSES_MESSAGE, null);
                    }
                    return response()->custom(400, Constant::ERROR_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function search(Request $request)
    {
        if ($request->has('token')) {
            $response = MainModuleService::auth($request->get('token'), false);
            if (!$response['error']) {
                switch ($response['body']['entity']['role'])
                {
                    case "admin":
                        if($request->has('status') && $request->status != 4)
                        {
                            $courses = Course::Search($request->keyword)->with('image', 'teacher.teacherable', 'students', 'certificate')->where('status', $request->status)->latest()->paginate(10);
                        } else {
                            $courses = Course::Search($request->keyword)->with('image', 'teacher.teacherable', 'students', 'certificate')->latest()->paginate(10);
                        }
                        foreach ($courses as $course)
                        {
                            $course->canEdit = true;
                        }
                        break;
                    case "teacher":
                        $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                        $courses = Course::Search($request->keyword)->with('image', 'teacher.teacherable', 'students', 'certificate')->where([['teacher_id', '!=', $teacher->id], ['status', '<', 3]])->latest()->paginate(10);
                        foreach ($courses as $course)
                        {
                            $course->canEdit = false;
                        }
                        break;
                    default:
                        $courses = Course::Search($request->keyword)->with('image', 'teacher.teacherable', 'students', 'certificate')->where('status', '<', 3)->latest()->paginate(10);
                        foreach ($courses as $course)
                        {
                            $course->canEdit = false;
                        }
                }
                if (count($courses)) {
                    return response()->custom(200, Constant::SUCCESS_GET_COURSES_MESSAGE, $courses);
                }
                $data['data'] = null;
                return response()->custom(200, Constant::SUCCESS_GET_COURSES_MESSAGE, $data);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function searchMyCourses(Request $request)
    {
        if ($request->has('token')) {
            $response = MainModuleService::auth($request->get('token'), false);
            if (!$response['error']) {
                if ($response['body']['entity']['role'] == 'teacher') {
                    $teacher = Teacher::where([['teacherable_id', $response['body']['entity']['id']], ['teacherable_type', $response['body']['entity']['model']]])->first();
                    if ($teacher) {
                        $courses = Course::Search($request->keyword)->with('image', 'teacher.teacherable', 'students', 'certificate', 'applicants')->where('teacher_id', $teacher->id)->latest()->paginate(10);
                        foreach ($courses as $course)
                        {
                            $course->canEdit = true;
                        }
                        if (count($courses)) {
                            return response()->custom(200, Constant::SUCCESS_GET_COURSES_MESSAGE, $courses);
                        }
                        $data['data'] = null;
                        return response()->custom(200, Constant::SUCCESS_GET_COURSES_MESSAGE, $data);
                    }
                    return response()->custom(400, Constant::ERROR_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function applyCourse(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                if($response['body']['entity']['role'] == 'student' || ($response['body']['entity']['role'] == 'teacher' && $response['body']['entity']['model'] == Constant::STUDENT_IDENTIFIER))
                {
                    if($response['body']['entity']['role'] == 'student') {
                        $application = CourseApplicant::where([['student_id', $response['body']['entity']['id']], ['course_id', $id]])->first();
                    } else {
                        $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                        $application = CourseApplicant::where([['student_id', $teacher->teacherable_id], ['course_id', $id]])->first();
                    }

                    if(!$application)
                    {
                        $course = Course::where('id', $id)->first();
                        if($response['body']['entity']['role'] == 'student') {
                            $course->applicants()->attach($response['body']['entity']['id']);
                        } else {
                            $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                            $course->applicants()->attach($teacher->teacherable_id);
                        }

                        $courseTeacher = null;
                        switch ($course->teacher->teacherable_type) {
                            case Constant::PROFESSOR_IDENTIFIER:
                                $courseTeacher = Professor::where('id', $course->teacher->teacherable_id)->first();
                                break;
                            case Constant::STUDENT_IDENTIFIER:
                                $courseTeacher = Student::where('id', $course->teacher->teacherable_id)->first();
                                break;
                        }

                        if($courseTeacher->email) {
                            $contactMail = Contact::first();
                            Mail::send([], [], function ($message) use ($courseTeacher, $contactMail, $response, $course) {
                                $message->to($courseTeacher->email)
                                    ->subject("Aplikacija za pohadjanje kursa")
                                    ->from($contactMail->email)
                                    ->setBody("Poštovani, za kurs " . $course->name . " aplicirao je student "
                                        .$response['body']['entity']['first_name']." "
                                        .$response['body']['entity']['last_name'].". Možete pogledati prijavu i odobriti je na linku: "
                                        .Constant::COURSE_SINGLE_VIEW_ROUTE.$course->id);
                            });
                        }
                        return response()->custom(200, Constant::SUCCESS_APPLIED_COURSE_MESSAGE, $course);
                    }
                    return response()->custom(403, Constant::ALREADY_APPLIED_COURSE_MESSAGE, null);
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function removeApplicant(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $course = Course::find($id);
                switch ($response['body']['entity']['role'])
                {
                    case "admin":
                        $applicant = CourseApplicant::where([['student_id', $request->get('student_id')], ['course_id', $id]])->first();
                        if($applicant && $applicant->delete()) {
                            $course = Course::with('image', 'teacher.teacherable', 'students', 'certificate', 'applicants')->find($id);
                            $course->canApprove = true;
                            $course->canApply = false;
                            return response()->custom(200, Constant::SUCCESS_REMOVED_APPLICANT_MESSAGE, $course);
                        }
                        return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        break;
                    case "teacher":
                        $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                        if($course->teacher_id == $teacher->id) {
                            $applicant = CourseApplicant::where([['student_id', $request->get('student_id')], ['course_id', $id]])->first();
                            if($applicant && $applicant->delete()) {
                                $course = Course::with('image', 'teacher.teacherable', 'students', 'certificate', 'applicants')->find($id);
                                $course->canApprove = true;
                                $course->canApply = false;
                                return response()->custom(200, Constant::SUCCESS_REMOVED_APPLICANT_MESSAGE, $course);
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        }
                        return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                        break;
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }

    public function addApplicant(Request $request, $id)
    {
        if($request->has('token'))
        {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $course = Course::find($id);
                switch ($response['body']['entity']['role'])
                {
                    case "admin":
                        $course = Course::with('image', 'teacher.teacherable', 'students', 'certificate', 'applicants')->find($id);
                        $course->students()->attach($request->get('student_id'));
                        $applicant = CourseApplicant::where([['student_id', $request->get('student_id')], ['course_id', $id]])->first();
                        if($applicant && $applicant->delete()) {
                            $course = Course::with('image', 'teacher.teacherable', 'students', 'certificate', 'applicants')->find($id);
                            $course->canApprove = true;
                            $course->canApply = false;
                            return response()->custom(200, Constant::SUCCESS_ADDED_APPLICANT_MESSAGE, $course);
                        }
                        return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        break;
                    case "teacher":
                        $teacher = Teacher::where([['teacherable_type', $response['body']['entity']['model']], ['teacherable_id', $response['body']['entity']['id']]])->first();
                        if($course->teacher_id == $teacher->id) {
                            $course = Course::with('image', 'teacher.teacherable', 'students', 'certificate', 'applicants')->find($id);
                            $course->students()->attach($request->get('student_id'));
                            $applicant = CourseApplicant::where([['student_id', $request->get('student_id')], ['course_id', $id]])->first();
                            if($applicant && $applicant->delete()) {
                                $course = Course::with('image', 'teacher.teacherable', 'students', 'certificate', 'applicants')->find($id);
                                $course->canApprove = true;
                                $course->canApply = false;
                                return response()->custom(200, Constant::SUCCESS_ADDED_APPLICANT_MESSAGE, $course);
                            }
                            return response()->custom(400, Constant::ERROR_MESSAGE, null);
                        }
                        return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
                        break;
                }
                return response()->custom(403, Constant::WRONG_PERMISSION_MESSAGE, null);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }
}
