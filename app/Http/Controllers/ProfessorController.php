<?php

namespace App\Http\Controllers;

use App\Helpers\MainModuleService;
use App\Professor;
use Illuminate\Http\Request;

class ProfessorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getMainProfessors(Request $request) {
        if($request->has('token')) {
            $response = MainModuleService::auth($request->get('token'), false);
            if(!$response['error'])
            {
                $professorsResponse = MainModuleService::getProfessors($request->all());
                if($professorsResponse['body']['status'] == 200) {
                    $allProfessors = $professorsResponse['body']['entity']['data'];
                    $professors = [];
                    foreach ($allProfessors as $professor) {
                        $ourProfessor = Professor::where('user_id', $professor['id'])->first();
                        if(!$ourProfessor || ($ourProfessor->role != 'teacher' && $ourProfessor->role != 'admin')) {
                            array_push($professors, $professor);
                        }
                    }
                    $professorsResponse['body']['entity']['data'] = $professors;
                }
                return response()->custom($professorsResponse['body']['status'], $professorsResponse['body']['message'], $professorsResponse['body']['entity']);
            }
            return response()->custom($response['body']['status'], $response['body']['message'], $response['body']['entity']);
        }
        return response()->custom(401, Constant::UNAUTHORIZED_MESSAGE, null);
    }
}
