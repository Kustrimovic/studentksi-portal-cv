<?php

namespace App;

use App\Helpers\Constant;
use App\Helpers\ImageService;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'company_id', 'username', 'password' , 'name', 'lastname', 'email', 'role', 'company_name'
    ];

    public function ads()
    {
        return $this->hasMany('App\Ad');
    }

    public function suggestions() {
        return $this->morphMany('App\Suggestion', 'suggestionable');
    }

    public function logo()
    {
        return $this->morphOne('App\Image', 'imageable');
    }

    public function scopeSearch($query, $keyword)
    {
        if ($keyword!='') {
            $query->with('ads', 'logo')->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("lastname", "LIKE", "%$keyword%")
                    ->orWhere("username", "LIKE", "%$keyword%")
                    ->orWhere("company_name", "LIKE", "%$keyword%")
                    ->orWhere("email", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }

    public function delete()
    {
        $ads = Ad::where('company_id', $this->id)->get();
        if(count($ads))
        {
            foreach ($ads as $ad)
            {
                $ad->delete();
            }
        }

        $images = Image::where([['imageable_id', $this->id], ['imageable_type', Constant::COMPANY_IDENTIFIER]])->get();
        foreach ($images as $image)
        {
            if($image->path != Constant::POST_DEFAULT_IMAGE_PATH) {
                ImageService::remove($image->path, Constant::IMAGES_POSTS_ROOT_PATH, $this->id);
            }
            $image->delete();
        }
        return parent::delete();
    }
}
