<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $fillable = [
        'admineable_id', 'admineable_type'
    ];

    public function admineable()
    {
        return $this->morphTo();
    }

    public function suggestions()
    {
        return $this->morphMany('App\Suggestion', 'suggestionable');
    }
}
