<?php

namespace App;

use App\Helpers\Constant;
use App\Helpers\ImageService;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'name', 'description', 'from', 'to', 'status'
    ]; // Status value can be 1(not started), 2(pending), 3(finished)

    public function image()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Teacher');
    }

    public function students()
    {
        return $this->belongsToMany('App\Student', 'student_courses', 'course_id', 'student_id');
    }

    public function certificate()
    {
        return $this->belongsTo('App\Certificate');
    }

    public function applicants()
    {
        return $this->belongsToMany('App\Student', 'course_applicants', 'course_id', 'student_id');
    }

    public function scopeSearch($query, $keyword)
    {
        if ($keyword!='') {
            $query->with('image', 'teacher.teacherable', 'students', 'certificate')->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }

    function delete()
    {
        $images = Image::where([['imageable_id', $this->id], ['imageable_type', Constant::COURSE_IDENTIFIER]])->get();
        foreach ($images as $image) {
            if($image->path != Constant::COURSES_DEFAULT_IMAGE_PATH) {
                ImageService::remove($image->path, Constant::IMAGES_COURSES_ROOT_PATH, $this->id);
                $image->delete();
            }
        }
        $student_courses = StudentCourse::where([['course_id', $this->id]])->get();
        foreach ($student_courses as $item) {
            $item->delete();
        }
        $applicants = CourseApplicant::where([['course_id', $this->id]])->get();
        foreach ($applicants as $item) {
            $item->delete();
        }
        return parent::delete();
    }
}
