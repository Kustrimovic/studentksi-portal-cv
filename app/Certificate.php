<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $fillable = [
        'title', 'description'
    ];

    public function students()
    {
        return $this->belongsToMany('App\Student', 'student_certificates', 'certificate_id', 'student_id')
                    ->withPivot('rating');
    }

    public function scopeSearch($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("title", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }

    public function delete()
    {
        $courses = Course::where([['certificate_id', $this->id]])->get();
        foreach ($courses as $course) {
            $course->delete();
        }
        return parent::delete();
    }
}
