<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperAdmin extends Model
{
    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'username', 'email', 'role'
    ];

    public function admin()
    {
        return $this->morphOne('App\Admin', 'admineable');
    }
}
