<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'role'
    ];

    public function teacher()
    {
        return $this->morphOne('App\Teacher', 'teacherable');
    }

    public function admin()
    {
        return $this->morphOne('App\Admin', 'admineable');
    }
}
