<?php

namespace App;

use App\Helpers\Constant;
use App\Helpers\ImageService;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'date_of_birth', 'email', 'city', 'education', 'employment', 'workplace', 'linkedin_url', 'role'
    ];

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Course', 'student_courses', 'student_id', 'course_id');
    }

    public function applications()
    {
        return $this->belongsToMany('App\Ad', 'applicants', 'student_id', 'ad_id');
    }

    public function courseapplications()
    {
        return $this->belongsToMany('App\Course', 'course_applicants', 'student_id', 'course_id');
    }

    public function certificates()
    {
        return $this->belongsToMany('App\Certificate', 'student_certificates', 'student_id', 'certificate_id')
                    ->withPivot('rating');
    }

    public function suggestions()
    {
        return $this->hasMany('App\Suggestion');
    }

    public function teacher()
    {
        return $this->morphOne('App\Teacher', 'teacherable');
    }

    public function admin()
    {
        return $this->morphOne('App\Admin', 'admineable');
    }

    public function scopeSearch($query, $keyword)
    {
        if ($keyword!='') {
            $query->with('image', 'courses', 'certificates', 'suggestions')->where(function ($query) use ($keyword) {
                $query->where("first_name", "LIKE","%$keyword%")
                    ->orWhere("last_name", "LIKE", "%$keyword%")
                    ->orWhere("email", "LIKE", "%$keyword%")
                    ->orWhere("city", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }

    public function delete()
    {
        $images = Image::where([['imageable_id', $this->id], ['imageable_type', Constant::STUDENT_IDENTIFIER]])->get();
        foreach ($images as $image)
        {
            if($image->path != Constant::DEFAULT_USER_IMAGE_PATH) {
                ImageService::remove($image->path, Constant::USER_IMAGES_ROOT_PATH, $this->id);
            }
            $image->delete();
        }
        $student_courses = StudentCourse::where([['student_id', $this->id]])->get();
        foreach ($student_courses as $item) {
            $item->delete();
        }
        $applications = Applicant::where([['student_id', $this->id]])->get();
        foreach ($applications as $item) {
            $item->delete();
        }
        $suggestions = Suggestion::where([['student_id', $this->id]])->get();
        foreach ($suggestions as $suggestion) {
            $suggestion->delete();
        }
        return parent::delete();
    }
}
