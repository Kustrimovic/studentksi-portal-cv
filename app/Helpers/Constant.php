<?php

namespace App\Helpers;

class Constant
{
    const ENDPOINT_API_URL = "http://vtsnis.edu.rs/vtsportal/api/";
    const CV_PORTAL_ENDPOINT = "http://localhost:8020/";
    const STUDENT_SINGLE_VIEW_ROUTE = Constant::CV_PORTAL_ENDPOINT."#/single-view-student/";
    const COURSE_SINGLE_VIEW_ROUTE = Constant::CV_PORTAL_ENDPOINT."#/single-view-courses/";
    const CHECK_USER_API = "check/token";
    const CREATE_COMPANY_API = "company/create";
    const DESTROY_COMPANY_API = "company/delete";
    const UPDATE_COMPANY_API = "company/update/";
    const GET_PROFESSORS_API = "teachers";

    const IMAGES_ROOT_PATH = "images/uploads/";
    const USER_IMAGES_ROOT_PATH = Constant::IMAGES_ROOT_PATH.'users/';
    const IMAGES_COURSES_ROOT_PATH = Constant::IMAGES_ROOT_PATH."courses/";
    const IMAGES_POSTS_ROOT_PATH = Constant::IMAGES_ROOT_PATH."posts/";
    const IMAGES_COMPANIES_ROOT_PATH = Constant::IMAGES_ROOT_PATH."companies/";
    const DEFAULT_USER_IMAGE_PATH =  Constant::USER_IMAGES_ROOT_PATH.'user-avatar.jpg';
    const POST_DEFAULT_IMAGE_PATH = Constant::IMAGES_POSTS_ROOT_PATH."post-default-image.png";
    const COURSES_DEFAULT_IMAGE_PATH = Constant::IMAGES_COURSES_ROOT_PATH."courses-default-image.png";
    const COMPANY_DEFAULT_IMAGE_PATH = Constant::IMAGES_COMPANIES_ROOT_PATH."company-default-image.png";

    const COURSE_IDENTIFIER = 'App\Course';
    const POST_IDENTIFIER = 'App\Post';
    const STUDENT_IDENTIFIER = 'App\Student';
    const PROFESSOR_IDENTIFIER = 'App\Professor';
    const TEACHER_IDENTIFIER = 'App\Teacher';
    const ADMIN_IDENTIFIER = 'App\Admin';
    const COMPANY_IDENTIFIER = 'App\Company';
    const SUPER_ADMIN_IDENTIFIER = 'App\SuperAdmin';

    // Messages
    const UNAUTHORIZED_MESSAGE = "Neovlašćen pristup";
    const WRONG_PERMISSION_MESSAGE = 'Nemate permisiju za ovu aktivnost';
    const ERROR_MESSAGE = "Došlo je do greške";
    const SUCCESS_EDIT_MESSAGE = 'Uspešno izvršene izmene';
    const SUCCESS_DELETE_MESSAGE = 'Uspešno brisanje';

    // Ads messages
    const SUCCESS_GET_ADS_MESSAGE = "Uspešno preuzeta lista oglasa";
    const SUCCESS_CREATED_AD_MESSAGE = "Uspešno kreiran oglas";
    const SUCCESS_GET_AD_MESSAGE = 'Uspešno preuzet oglas';
    const AD_NOT_FOUND = 'Oglas nije pronađen';
    const SUCCESS_APPLIED_MESSAGE = 'Uspešno ste aplicirali na oglas';
    const ALREADY_APPLIED_MESSAGE = 'Već ste aplicirali na ovaj oglas';

    // Sertificates messages
    const SUCCESS_GET_CERTIFICATES_MESSAGE = "Uspešno preuzeta lista sertifikata";
    const SUCCESS_GET_CERTIFICATE_MESSAGE = "Uspešno preuzet sertifikat";
    const SUCCESS_CREATED_CERTIFICATE_MESSAGE = "Uspešno kreiran sertifikat";
    const CERTIFICATE_NOT_FOUND_MESSAGE = 'Sertifikat nije pronađen';

    // Companies messages
    const SUCCESS_GET_COMPANIES_MESSAGE = 'Uspešno preuzeta lista kompanija';
    const SUCCESS_GET_COMPANY_MESSAGE = 'Uspešno preuzeta kompanija';
    const SUCCESS_CREATED_COMPANY_MESSAGE = 'Uspešno kreiran nalog kompanije';
    const COMPANY_NOT_FOUND_MESSAGE = 'Kompanija nije pronađena';

    // Contacts messages
    const SUCCESS_GET_LIST_EMAILS_MESSAGE = 'Uspešno preuzet kontakt email';
    const SUCCESS_CONTACT_MESSAGE = 'Uspešno ste kontaktirali administratore';

    // Courses messages
    const SUCCESS_GET_COURSES_MESSAGE = 'Uspešno preuzeta lista kurseva';
    const SUCCESS_GET_COURSE_MESSAGE = 'Uspešno preuzet kurs';
    const SUCCESS_CREATED_COURSE_MESSAGE = 'Uspešno ste kreirali kurs';
    const COURSE_NOT_FOUND_MESSAGE = 'Kurs nije pronađen';
    const SUCCESS_DELETE_COURSE_MESSAGE = 'Uspešno obrisan kurs';
    const SUCCESS_COMPLETED_COURSE_MESSAGE = 'Uspešno završen kurs';
    const ALREADY_COMPLETED_COURSE_MESSAGE = 'Kurs je već završen';
    const CERTIFICATE_FOR_COURSE_NOT_FOUND = 'Sertifikat za ovaj kurs nije pronađen';
    const SUCCESS_APPLIED_COURSE_MESSAGE = 'Uspešno ste aplicirali da pohađate kurs';
    const ALREADY_APPLIED_COURSE_MESSAGE = 'Već ste aplicirali da pohađate ovaj kurs';
    const SUCCESS_REMOVED_APPLICANT_MESSAGE = 'Uspešno ste uklonili prijavu';
    const SUCCESS_ADDED_APPLICANT_MESSAGE = 'Uspešno ste dodali studenta da pohađa kurs';

    // Posts messages
    const SUCCESS_GET_POSTS_MESSAGE = 'Uspešno preuzeta lista novosti';
    const SUCCESS_GET_POST_MESSAGE = 'Uspešno pruzeta novost';
    const SUCCESS_CREATED_POST_MESSAGE = 'Uspešno ste kreirali novost';
    const SUCCESS_DELETED_POST_MESSAGE = 'Uspešno ste obrisali novost';
    const POST_NOT_FOUND_MESSAGE = 'Novost nije pronađena';

    // Students messages
    const SUCCESS_GET_STUDENTS_MESSAGE = 'Uspešno preuzeta lista studenata';
    const SUCCESS_GET_STUDENT_MESSAGE = 'Uspešno preuzet CV studenta';
    const SUCCESS_CREATED_STUDENT_CV_MESSAGE = 'Uspešno ste kreirali CV';
    const SUCCESS_DELETED_STUDENT_CV_MESSAGE = 'Uspešno ste obrisali CV';
    const ALREADY_CREATED_STUDENT_CV_MESSAGE = 'CV je već kreiran';
    const STUDENT_NOT_FOUND_MESSAGE = 'CV nije pronađen';
    const SUCCESS_GET_STUDENTS_ROLE_STUDENT_MESSAGE = "Uspešno preuzeta lista studenata sa rolom 'student'";
    const STUDENT_WITH_ROLE_STUDENT_NOT_FOUND_MESSAGE = "Nema korisnika sa rolom 'student'";
    const SUCCESS_GET_STUDENTS_CAN_LISTEN_COURSE_MESSAGE = "Uspešno preuzeta lista studenata koji mogu da slušaju kurs";
    const STUDENT_CAN_LISTEN_COURSE_NOT_FOUND_MESSAGE = "Nema studenta koji može da sluša ovaj kurs";

    // Suggestions messages
    const SUCCESS_GET_SUGGESTIONS_MESSAGE = 'Uspešno preuzeta lista preporuka';
    const SUCCESS_GET_SUGGESTION_MESSAGE = 'Uspešno preuzeta preporuka';
    const SUCCESS_CREATED_SUGGESTION_MESSAGE = 'Uspešno ste kreirali preporuku';
    const SUCCESS_DELETED_SUGGESTION_MESSAGE = 'Uspešno ste obrisali preporuku';
    const SUGGESTION_NOT_FOUND_MESSAGE = 'Preporuka nije pronađena';

    // Teachers messages
    const SUCCESS_GET_TEACHERS_MESSAGE = 'Uspešno preuzeta lista predavača';
    const SUCCESS_ADDED_STUDENT_AS_TEACHER_MESSAGE = 'Uspešno prebačen student u predavače';
    const SUCCESS_ADDED_PROFESSOR_AS_TEACHER_MESSAGE = 'Uspešno prebačen profesor u predavače';
    const USER_CANT_BE_A_TEACHER_MESSAGE = 'Korisnik ne može biti predavač';
    const SUCCESS_REMOVED_TEACHER_ROLE_MESSAGE = 'Uspešno uklonjena rola predavača';
    const TEACHER_NOT_FOUND_MESSAGE = 'Predavač nije pronađen';

    // Admins messages
    const SUCCESS_GET_ADMINS_MESSAGE = 'Uspešno preuzeta lista admina';
    const SUCCESS_ADDED_STUDENT_AS_ADMIN_MESSAGE = 'Uspešno prebačen student u admine';
    const SUCCESS_ADDED_TEACHER_AS_ADMIN_MESSAGE = 'Uspešno prebačen predavač u admine';
    const SUCCESS_ADDED_PROFESSOR_AS_ADMIN_MESSAGE = 'Uspešno prebačen profesor u admine';
    const SUCCESS_REMOVED_ADMIN_ROLE_MESSAGE = 'Uspešno uklonjena rola admina';
    const ADMIN_NOT_FOUND_MESSAGE = 'Admin nije pronađen';

}