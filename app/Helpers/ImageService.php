<?php

namespace App\Helpers;


use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageService
{
    public static function upload($image, $rootPath, $id, $fileName)
    {
        if (!file_exists(public_path($rootPath.$id))) {
            mkdir(public_path($rootPath.$id), 777, true);
        }
        $path = $rootPath.$id."/".Carbon::now()->timestamp.$fileName;
        Image::make($image)->save($path);
        return $path;
    }

    public static function remove($path, $rootPath, $id)
    {
        Storage::delete($path);
        $files = Storage::allFiles($rootPath.$id);
        if(!$files) {
            File::deleteDirectory(public_path($rootPath.$id));
        }
    }
}