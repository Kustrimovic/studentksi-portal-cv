<?php
/**
 * Created by PhpStorm.
 * User: darko
 * Date: 22.8.17.
 * Time: 17.38
 */

namespace App\Helpers;

use App\Admin;
use App\Company;
use App\Professor;
use App\Student;
use App\SuperAdmin;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;


class MainModuleService
{
    public static function auth($token, $isOnlyCheck)
    {
        try {
            $http = new Client(['base_uri' => Constant::ENDPOINT_API_URL]);
            $response = $http->get(Constant::CHECK_USER_API.'?token='.$token);
            $body = json_decode($response->getBody(), TRUE);
            switch ($body['entity']['role'])
            {
                case 'student':
                    $student = Student::where('user_id', $body['entity']['id'])->first();
                    $newBody['status'] = 200;
                    if(!$student)
                    {
                        $newBody['entity'] = $body['entity'];
                        $newBody['message'] = "User didn't created CV yet";
                        $data = array('body' => $newBody, 'error' => true);
                        if($isOnlyCheck)
                        {
                            $data = array('body' => $newBody, 'error' => false);
                        }
                        return $data;
                    }
                    $newBody['entity'] = $student;
                    $newBody['entity']['model'] = Constant::STUDENT_IDENTIFIER;
                    if($isOnlyCheck)
                    {
                        $newBody['entity']['id'] = $student->user_id;
                    }
                    $newBody['message'] = "Successfully get student data";
                    $data = array('body' => $newBody, 'error' => false);
                    return $data;
                    break;
                case 'company':
                    $company = Company::where('company_id', $body['entity']['id'])->first();
                    $newBody['status'] = 200;
                    if(!$company)
                    {
                        $newBody['entity'] = $body['entity'];
                        $newBody['message'] = "Company doesn't exist in CV database";
                        $data = array('body' => $newBody, 'error' => true);
                        return $data;
                    }
                    $newBody['entity'] = $company;
                    $newBody['message'] = "Successfully get company data";
                    $data = array('body' => $newBody, 'error' => false);
                    return $data;
                    break;
                case 'professor':
                    $professor = Professor::where('user_id', $body['entity']['id'])->first();
                    $newBody['status'] = 200;
                    if(!$professor)
                    {
                        $newBody['entity'] = $body['entity'];
                        $newBody['message'] = "Successfully get professor data";
                        $data = array('body' => $newBody, 'error' => false);
                        return $data;
                    }
                    $newBody['entity'] = $professor;
                    $newBody['entity']['model'] = Constant::PROFESSOR_IDENTIFIER;
                    $newBody['message'] = "Successfully get professor data";
                    $data = array('body' => $newBody, 'error' => false);
                    return $data;
                    break;
                case 'admin':
                    $superAdmin = SuperAdmin::where('user_id', $body['entity']['id'])->first();
                    if(!$superAdmin)
                    {
                        $superAdmin = new SuperAdmin();
                        $superAdmin->user_id = $body['entity']['id'];
                        $superAdmin->username = $body['entity']['username'];
                        $superAdmin->first_name = $body['entity']['name'];
                        $superAdmin->last_name = $body['entity']['lastname'];
                        $superAdmin->email = $body['entity']['email'];
                        $superAdmin->role = $body['entity']['role'];
                        $superAdmin->save();
                        $superAdmin = $superAdmin = SuperAdmin::where('user_id', $body['entity']['id'])->first();

                        $admin = new Admin();
                        $admin->admineable_id = $superAdmin->id;
                        $admin->admineable_type = Constant::SUPER_ADMIN_IDENTIFIER;
                        $admin->save();
                    }
                    $newBody['status'] = 200;
                    $newBody['message'] = "Successfully get admin data";
                    $newBody['entity'] = $superAdmin;
                    $newBody['entity']['model'] = Constant::SUPER_ADMIN_IDENTIFIER;
                    $data = array('body' => $newBody, 'error' => false);
                    return $data;
            }
            $data = array('body' => $body, 'error' => false);
            return $data;
        } catch (RequestException $e) {
            $body = json_decode($e->getResponse()->getBody(), TRUE);
            $data = array('body' => $body, 'error' => true);
            return $data;
        }
    }

    public static function storeCompany($data)
    {
        try {
            $http = new Client(['base_uri' => Constant::ENDPOINT_API_URL]);
            $response = $http->post(Constant::CREATE_COMPANY_API.'?token='.$data['token'],
                [
                    'form_params' => $data
                ]);
            $body = json_decode($response->getBody(), TRUE);
            $data = array('body' => $body, 'error' => false);
            return $data;
        } catch (RequestException $e) {
            $body = json_decode($e->getResponse()->getBody(), TRUE);
            $data = array('body' => $body, 'error' => true);
            return $data;
        }
    }

    public static function updateCompany($data, $id)
    {
        try {
            $http = new Client(['base_uri' => Constant::ENDPOINT_API_URL]);
            $response = $http->post(Constant::UPDATE_COMPANY_API.$id.'?token='.$data['token'],
                [
                    'form_params' => $data
                ]);
            $body = json_decode($response->getBody(), TRUE);
            $data = array('body' => $body, 'error' => false);
            return $data;
        } catch (RequestException $e) {
            $body = json_decode($e->getResponse()->getBody(), TRUE);
            $data = array('body' => $body, 'error' => true);
            return $data;
        }
    }

    public static function destroyCompany($token, $id)
    {
        try {
            $http = new Client(['base_uri' => Constant::ENDPOINT_API_URL]);
            $response = $http->post(Constant::DESTROY_COMPANY_API.'?token='.$token,
                [
                    'form_params' => [
                        'company_id' => $id
                    ]
                ]);
            $body = json_decode($response->getBody(), TRUE);
            $data = array('body' => $body, 'error' => false);
            return $data;
        } catch (RequestException $e) {
            $body = json_decode($e->getResponse()->getBody(), TRUE);
            $data = array('body' => $body, 'error' => true);
            return $data;
        }
    }

    public static function getProfessors($data)
    {
        try {
            $http = new Client(['base_uri' => Constant::ENDPOINT_API_URL]);
            $response = $http->get(Constant::GET_PROFESSORS_API.'?page='.$data['page'].'&token='.$data['token'],
                [
                    'form_params' => $data
                ]);
            $body = json_decode($response->getBody(), TRUE);
            $data = array('body' => $body, 'error' => false);
            return $data;
        } catch (RequestException $e) {
            $body = json_decode($e->getResponse()->getBody(), TRUE);
            $data = array('body' => $body, 'error' => true);
            return $data;
        }
    }
}