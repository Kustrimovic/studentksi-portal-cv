<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $fillable = [
        'title', 'description', 'category'
    ];

    public function company() {
        return $this->belongsTo('App\Company');
    }

    public function applicants()
    {
        return $this->belongsToMany('App\Student', 'applicants', 'ad_id', 'student_id');
    }

    public function scopeSearch($query, $keyword)
    {
        if ($keyword!='') {
            $query->with('company.logo')->where(function ($query) use ($keyword) {
                $query->where("title", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }

    function delete()
    {
        $applicants = Applicant::where([['ad_id', $this->id]])->get();
        foreach ($applicants as $item) {
            $item->delete();
        }
        return parent::delete();
    }
}
