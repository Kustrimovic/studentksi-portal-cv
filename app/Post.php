<?php

namespace App;

use App\Helpers\Constant;
use App\Helpers\ImageService;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'description'];

    public function images()
    {
        return $this->morphOne('App\Image', 'imageable');
    }

    public function scopeSearch($query, $keyword)
    {
        if ($keyword!='') {
            $query->with('images')->where(function ($query) use ($keyword) {
                $query->where("title", "LIKE","%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }

    public function delete()
    {
        $images = Image::where([['imageable_id', $this->id], ['imageable_type', Constant::POST_IDENTIFIER]])->get();
        foreach ($images as $image)
        {
            if($image->path != Constant::POST_DEFAULT_IMAGE_PATH) {
                ImageService::remove($image->path, Constant::IMAGES_POSTS_ROOT_PATH, $this->id);
            }
            $image->delete();
        }
        return parent::delete();
    }
}
