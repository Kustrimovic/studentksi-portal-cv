<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $fillable = [
        'title', 'description', 'suggestionable_id', 'suggestionable_type'
    ];

    public function student()
    {
        return $this->belongsTo('App\Student');
    }

    public function suggestionable()
    {
        return $this->morphTo();
    }
}
