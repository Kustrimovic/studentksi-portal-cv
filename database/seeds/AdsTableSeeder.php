<?php

use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['job', 'internship'];
        $faker = Faker\Factory::create();
        for($i = 0; $i < 12; $i++)
        {
            DB::table('ads')->insert([
                'company_id' => $i+1,
                'title' => $faker->jobTitle,
                'description' => $faker->text(100),
                'category' => $categories[rand(0, 1)],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
