<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = array(
            array(
                "name" => "Android",
                "description" => "Kurs o Android Developmentu"
            ),
            array(
                "name" => "Java",
                "description" => "Kurs o Java Developmentu"
            ),
            array(
                "name" => "Swift",
                "description" => "Kurs o Swift Developmentu"
            ),
            array(
                "name" => "Php",
                "description" => "Kurs o Php Developmentu"
            ),
            array(
                "name" => "AngularJs",
                "description" => "Kurs o AngularJs Developmentu"
            ),
            array(
                "name" => "Laravel",
                "description" => "Kurs o Laravel Developmentu"
            )
        );

        $faker = Faker\Factory::create();
        $teachers = \App\Teacher::all();
        foreach ($courses as $course)
        {
            DB::table('courses')->insert([
                'teacher_id' => rand(1, count($teachers)),
                'name' => $course["name"],
                'description' => $course["description"],
                'from' => $faker->dateTimeBetween('-1 year', 'now'),
                'to' => $faker->dateTimeBetween('now', '1 year'),
                'status' => rand(1, 3),
                'certificate_id' => rand(1, 10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
