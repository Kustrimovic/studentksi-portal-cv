<?php

use Illuminate\Database\Seeder;

class ProfessorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $roleArray = ['teacher', 'admin'];
        DB::table('professors')->insert([
            'user_id' => 958,
            'first_name' => "Miloš",
            'last_name' => "Kosanović",
            'role' => 'admin',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        for($i = 0; $i < 10; $i++)
        {
            DB::table('professors')->insert([
                'user_id' => $faker->unique()->numberBetween(1000, 10000),
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'role' => $roleArray[rand(0, 1)],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
