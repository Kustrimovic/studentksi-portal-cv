<?php

use Illuminate\Database\Seeder;
use \App\Helpers\Constant;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = array(
            'android.png',
            'java.png',
            'swift.png',
            'php.png',
            'angular.png',
            'laravel.png'
        );

        foreach ($images as $index=>$image)
        {
            DB::table('images')->insert([
                'imageable_id' => $index + 1,
                'imageable_type' => Constant::COURSE_IDENTIFIER,
                'path' => Constant::IMAGES_COURSES_ROOT_PATH.$image,
                'created_at' => \Carbon\Carbon::now()
            ]);
        }

        for($i = 0; $i < 10; $i++)
        {
            DB::table('images')->insert([
                'imageable_id' => $i + 1,
                'imageable_type' => Constant::POST_IDENTIFIER,
                'path' => Constant::POST_DEFAULT_IMAGE_PATH,
                'created_at' => \Carbon\Carbon::now()
            ]);
        }

        for($i = 1; $i < 14; $i++)
        {
            DB::table('images')->insert([
                'imageable_id' => $i,
                'imageable_type' => Constant::STUDENT_IDENTIFIER,
                'path' => Constant::DEFAULT_USER_IMAGE_PATH,
                'created_at' => \Carbon\Carbon::now()
            ]);
        }

        $companies = \App\Company::all();
        foreach ($companies as $company)
        {
            DB::table('images')->insert([
                'imageable_id' => $company->id,
                'imageable_type' => Constant::COMPANY_IDENTIFIER,
                'path' => Constant::COMPANY_DEFAULT_IMAGE_PATH,
                'created_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
