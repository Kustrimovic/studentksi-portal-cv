<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = \App\Student::where('role', 'admin')->get();
        $professors = \App\Professor::where('role', 'admin')->get();
        $superAdmins = \App\SuperAdmin::all();

        foreach ($students as $student)
        {
            DB::table('admins')->insert([
                'admineable_id' => $student->id,
                'admineable_type' => \App\Helpers\Constant::STUDENT_IDENTIFIER,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }

        foreach ($professors as $professor)
        {
            DB::table('admins')->insert([
                'admineable_id' => $professor->id,
                'admineable_type' => \App\Helpers\Constant::PROFESSOR_IDENTIFIER,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }

        foreach ($superAdmins as $superAdmin)
        {
            DB::table('admins')->insert([
                'admineable_id' => $superAdmin->id,
                'admineable_type' => \App\Helpers\Constant::SUPER_ADMIN_IDENTIFIER,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
