<?php

use Illuminate\Database\Seeder;

class TeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = \App\Student::where('role', 'teacher')->get();
        $professors = \App\Professor::where('role', 'teacher')->get();

        foreach ($students as $student)
        {
            DB::table('teachers')->insert([
                'teacherable_id' => $student->id,
                'teacherable_type' => \App\Helpers\Constant::STUDENT_IDENTIFIER,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }

        foreach ($professors as $professor)
        {
            DB::table('teachers')->insert([
                'teacherable_id' => $professor->id,
                'teacherable_type' => \App\Helpers\Constant::PROFESSOR_IDENTIFIER,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
