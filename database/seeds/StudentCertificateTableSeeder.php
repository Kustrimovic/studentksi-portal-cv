<?php

use Illuminate\Database\Seeder;

class StudentCertificateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = \App\Student::all();
        $certificates = \App\Certificate::all();

        for($i = 0; $i < 20; $i++) {
            DB::table('student_certificates')->insert([
                'student_id' => $students[rand(0, count($students) - 1)]->id,
                'certificate_id' => $students[rand(0, count($certificates) - 1)]->id,
                'rating' => rand(1, 10)
            ]);
        }
    }
}
