<?php

use Illuminate\Database\Seeder;

class SuggestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teachers = \App\Teacher::all();
        $companies = \App\Company::all();
        $admins = \App\Admin::all();
        $faker = Faker\Factory::create();

        foreach ($teachers as $teacher)
        {
            DB::table('suggestions')->insert([
                'student_id' => rand(1, 7),
                'title' => 'Suggestion for '.$faker->jobTitle,
                'description' => $faker->text(100),
                'suggestionable_id' => $teacher->id,
                'suggestionable_type' => \App\Helpers\Constant::TEACHER_IDENTIFIER,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }

        foreach ($companies as $company)
        {
            DB::table('suggestions')->insert([
                'student_id' => rand(1, 7),
                'title' => 'Suggestion for '.$faker->jobTitle,
                'description' => $faker->text(100),
                'suggestionable_id' => $company->id,
                'suggestionable_type' => \App\Helpers\Constant::COMPANY_IDENTIFIER,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }

        foreach ($admins as $admin)
        {
            DB::table('suggestions')->insert([
                'student_id' => rand(1, 7),
                'title' => 'Suggestion for '.$faker->jobTitle,
                'description' => $faker->text(100),
                'suggestionable_id' => $admin->id,
                'suggestionable_type' => \App\Helpers\Constant::ADMIN_IDENTIFIER,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
