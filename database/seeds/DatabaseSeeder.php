<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StudentsTableSeeder::class);
        $this->call(ProfessorsTableSeeder::class);
        $this->call(TeachersTableSeeder::class);
        $this->call(SuperAdminsTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(AdsTableSeeder::class);
        $this->call(SuggestionsTableSeeder::class);
        $this->call(CertificatesTableSeeder::class);
        $this->call(CoursesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(ImagesTableSeeder::class);
        $this->call(StudentCoursesTableSeeder::class);
        $this->call(StudentCertificateTableSeeder::class);
        $this->call(ApplicantsTableSeeder::class);
        $this->call(ContactTableSeeder::class);
    }
}
