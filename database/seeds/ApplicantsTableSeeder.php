<?php

use Illuminate\Database\Seeder;

class ApplicantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = \App\Student::all();
        $ads = \App\Course::all();

        for($i = 0; $i < 20; $i++)
        {
            DB::table('applicants')->insert([
                'student_id' => $students[rand(0, count($students) - 1)]->id,
                'ad_id' => $ads[rand(0, count($ads) - 1)]->id
            ]);
        }
    }
}
