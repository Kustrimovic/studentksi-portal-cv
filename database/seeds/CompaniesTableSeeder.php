<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i < 10; $i++)
        {
            DB::table('companies')->insert([
                'company_id' => $faker->unique()->numberBetween(2000, 5000),
                'username' => $faker->unique()->userName,
                'name' => $faker->name,
                'lastname' => $faker->lastName,
                'email' => $faker->unique()->email,
                'company_name' => $faker->company,
                'role' => 'company',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }

        DB::table('companies')->insert([
            'company_id' => 987,
            'username' => 'vtskompanija',
            'name' => 'Pera',
            'lastname' => 'Peric',
            'email' => 'pera@gmail.com',
            'company_name' => 'VtsKompanija.io',
            'role' => 'company',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('companies')->insert([
            'company_id' => 5500,
            'username' => 'microsoft',
            'name' => 'Microsoft',
            'lastname' => 'Company',
            'email' => 'kostic.darko95@gmail.com',
            'company_name' => 'microsoft.io',
            'role' => 'company',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('companies')->insert([
            'company_id' => 5500,
            'username' => 'apple',
            'name' => 'Apple',
            'lastname' => 'Apple',
            'email' => 'darkotuning1943@gmail.com',
            'company_name' => 'apple.io',
            'role' => 'company',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
