<?php

use Illuminate\Database\Seeder;

class StudentCoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = \App\Student::all();
        $courses = \App\Course::where('status','!=', 3)->get();

        for($i = 0; $i < 20; $i++)
        {
            DB::table('student_courses')->insert([
                'student_id' => $students[rand(0, count($students) - 1)]->id,
                'course_id' => $courses[rand(0, count($courses) - 1)]->id
            ]);
        }
    }
}
