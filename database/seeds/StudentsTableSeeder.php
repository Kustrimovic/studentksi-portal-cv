<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $roleArray = ['student', 'teacher'];
        for($i = 0; $i < 10; $i++)
        {
            DB::table('students')->insert([
                'user_id' => $faker->unique()->numberBetween(1000, 10000),
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'date_of_birth' => $faker->date(),
                'email' => $faker->unique()->email,
                'city' => $faker->city,
                'education' => "Education",
                'employment' => "Employment",
                'workplace' => "Workplace",
                'linkedin_url' => "https://www.linkedin.com/feed/",
                'role' => $roleArray[rand(0, 1)],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }

        DB::table('students')->insert([
            'user_id' => 82,
            'first_name' => "Darko",
            'last_name' => "Kostic",
            'date_of_birth' => $faker->date(),
            'email' => "kostic.darko95@gmail.com",
            'city' => "Prokuplje",
            'education' => "Tehnicka Skola 15. maj",
            'employment' => "Employment",
            'workplace' => "Workplace",
            'linkedin_url' => "https://www.linkedin.com/feed/",
            'role' => "admin",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('students')->insert([
            'user_id' => 79,
            'first_name' => "Darko",
            'last_name' => "Djordjevic",
            'date_of_birth' => $faker->date(),
            'email' => "darko.djordjevic@gmail.com",
            'city' => "Nis",
            'education' => "9. maj",
            'employment' => "Employment",
            'workplace' => "Workplace",
            'linkedin_url' => "https://www.linkedin.com/feed/",
            'role' => "teacher",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('students')->insert([
            'user_id' => 104,
            'first_name' => "Aleksandar",
            'last_name' => "Vukasinovic",
            'date_of_birth' => $faker->date(),
            'email' => "aleks@gmail.com",
            'city' => "Nis",
            'education' => "Tehnicka skola 15. maj",
            'employment' => "Employment",
            'workplace' => "Workplace",
            'linkedin_url' => "https://www.linkedin.com/feed/",
            'role' => "student",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
