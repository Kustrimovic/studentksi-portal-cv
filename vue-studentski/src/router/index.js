import Vue from 'vue'
import Router from 'vue-router'
import Token from '@/components/token'
import Students from '@/components/students'
import About from '@/components/about'
import Pocetna from '@/components/pocetna'
import SingleViewStudent from '@/components/single-view-student'
import SingleViewNovosti from '@/components/single-view-novosti'
import Kursevi from '@/components/kursevi'
import Sertifikati from '@/components/certificates'
import SingleViewCourses from '@/components/single-view-courses'
import CreateCV from '@/components/create-cv'
import Company from '@/components/company'
import SingleViewCompany from '@/components/single-view-company'
import SingleViewCertificates from '@/components/single-view-certificate'
import Suggestions from '@/components/suggestions'
import CompanyNews from '@/components/company-news'
import AccountEdit from '@/components/account-edit'
import SingleViewCompanyNews from '@/components/single-view-company-news'
import ListaKurseva from '@/components/lista-kurseva'
import MyCompanyNews from '@/components/my-company-news'
import CourseEdit from '@/components/kurs-edit'
import MyCourses from '@/components/moji-kursevi'
import MySuggestions from '@/components/my-suggestions'
import Settings from '@/components/settings'
import Contact from '@/components/contact'
import MyApplications from '@/components/my-applications'

Vue.use(Router);

export default new Router({
  routes: [
      {
          path: '/token/:token',
          name: 'Token',
          component: Token
      },
      {
          path: '/',
          name: 'Pocetna',
          component: Pocetna
      },
      {
          path: '/students',
          name: 'Students',
          component: Students
      },
      {
          path: '/about',
          name: 'About',
          component: About
      },
      {
          path: '/single-view-student/:id',
          name: 'single-view-student',
          component: SingleViewStudent
      },
      {
          path: '/single-view-novosti/:id',
          name: 'single-view-novosti',
          component: SingleViewNovosti
      },
      {
          path: '/kursevi',
          name: 'kursevi',
          component: Kursevi
      },
      {
          path: '/certificates',
          name: 'certificates',
          component: Sertifikati
      },
      {
          path: '/single-view-certificate/:id',
          name: 'single-view-certificate',
          component: SingleViewCertificates
      },
      {
          path: '/suggestions',
          name: 'suggestions',
          component: Suggestions
      },
      {
          path: '/single-view-courses/:id',
          name: 'single-view-courses',
          component: SingleViewCourses
      },
      {
          path: '/create-cv',
          name: 'create-cv',
          component: CreateCV
      },
      {
          path: '/company',
          name: 'company',
          component: Company
      },
      {
          path: '/account_edit',
          name: 'account_edit',
          component: AccountEdit
      },
      {
          path: '/single-view-company/:id',
          name: 'single-view-company',
          component: SingleViewCompany
      },
      {
          path: '/company-news',
          name: 'company-news',
          component: CompanyNews
      },
      {
          path: '/my-company-news',
          name: 'my-company-news',
          component: MyCompanyNews
      },
      {
          path: '/single-view-company-news/:id',
          name: 'single-view-company-news',
          component: SingleViewCompanyNews
      },
      {
          path: '/lista-kurseva',
          name: 'lista-kurseva',
          component: ListaKurseva
      },
      {
          path: '/single-view-kursa/:id',
          name: 'single-view-kursa',
          component: SingleViewCompanyNews
      },
      {
          path: '/edit-kursa/:id',
          name: 'edit-kursa',
          component: CourseEdit,
          beforeEnter: (to, from, next) => {
              if(window.role === 'company' || window.role === 'student') {
                  next('/');
              } else {
                  next();
              }
          }
      },
      {
          path: '/my-courses',
          name: 'my-courses',
          component: MyCourses
      },
      {
          path: '/my-suggestions',
          name: 'my-suggestions',
          component: MySuggestions
      },
      {
          path: '/settings',
          name: 'settings',
          component: Settings
      },
      {
          path: '/contact',
          name: 'contact',
          component: Contact
      },
      {
          path: '/my-applications',
          name: 'my-applications',
          component: MyApplications
      },
  ]
});
const router = new Router({});

router.beforeEach((to, from, next) => {
    console.log('Global -- beforeEach - fired');

    // re-route
    // Role: teacher
    if (to.path === '/kursevi' && window.role === 'teacher' || to.path === '/settings' && window.role === 'teacher' || to.path === '/my-company-news' && window.role === 'teacher' || to.path === '/create-cv' && window.role === 'teacher') {
        next('/');
    }
    // Role: admin
    else if (to.path === '/my-applications' && window.role === 'admin' || to.path === '/my-courses' && window.role === 'admin' || to.path === '/my-applications' && window.role === 'admin' || to.path === '/contact' && window.role === 'admin' || to.path === '/account_edit' && window.role === 'admin' || to.path === '/my-company-news' && window.role === 'admin' || to.path === '/create-cv' && window.role === 'admin') {
        next('/');
    }
    // Role: student
    else if (to.path === '/kursevi' && window.role === 'student' || to.path === '/settings' && window.role === 'student' || to.path === '/my-courses' && window.role === 'student' || to.path === '/suggestions' && window.role === 'student' || to.path === '/my-company-news' && window.role === 'student' || to.path === '/create-cv' && window.role === 'student') {
        next('/');
    }
    // Role: company
    else if (to.path === '/kursevi' && window.role === 'company' || to.path === '/settings' && window.role === 'company' || to.path === '/my-applications' && window.role === 'company' || to.path === '/my-courses' && window.role === 'company' || to.path === '/account_edit' && window.role === 'company' || to.path === '/create-cv' && window.role === 'company') {
        next('/');
    }
    else {
        next();
    }
});

// mount the app
const app = new Vue({
    router,
}).$mount("#app");