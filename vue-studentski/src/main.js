// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueLocalStorage from 'vue-localstorage';
import Toaster from 'v-toaster';
import pagination from "vuejs-uib-pagination";
import VeeValidate from 'vee-validate';

Vue.config.productionTip = false;

window.eventHub = new Vue();
window.axios = require('axios');
Vue.use(Toaster, {timeout: 5000});
Vue.use(VueLocalStorage);
Vue.use(pagination);
Vue.use(VeeValidate);
Vue.component('vue-simple-spinner', require('vue-simple-spinner'));
var VueTruncate = require('vue-truncate-filter');
Vue.use(VueTruncate);
window.endPoint = 'http://localhost:8000/';
window.api = endPoint + 'api/';

window.Toaster = require('v-toaster/dist/v-toaster.css');

window.role = null;
if(Vue.localStorage.get('role')) {
    window.role = Vue.localStorage.get('role');
    console.log(window.role);
}

axios.interceptors.request.use(function (config) {
    // console.log(config);
    if(Vue.localStorage.get('token')) {
        config.params = {
            token: Vue.localStorage.get('token')
        };
    }
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

axios.interceptors.response.use((response) => {
    console.log(response);
    if (response.data.message == "User didn\'t created CV yet") {
        Vue.localStorage.remove('role');
        router.push('../create-cv');
    } else {
        return response;
    }
}, function (error) {
    switch (error.response.status) {
        case 400:
            console.log('bad request');
            break;
        case 401:
            console.log('Unautorized');
            Vue.localStorage.remove('token');
            break;
    }
    return Promise.reject(error.response);
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});
