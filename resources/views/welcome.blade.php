<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>My app</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="/css/app.css">
        <!-- Styles -->
        
    </head>
    <body>
        <div id="app">
            <header class="mdl-layout__header mdl-layout__header--transparent">
                <div class="mdl-layout__header-row">
                    <!-- Title -->
                    <span class="mdl-layout-title"><img class="img-logo" src="images/vts-apps-team.png" alt=""></span>
                    <!-- Add spacer, to align navigation to the right -->
                    <div class="mdl-layout-spacer"></div>
                    <!-- Navigation -->
                    <nav class="mdl-navigation">
                        <router-link to="/" exact>
                            <a class="mdl-navigation__link">Pocetna</a>
                        </router-link>
                        <router-link to="/training">
                            <a class="mdl-navigation__link">Treninzi</a>
                        </router-link>
                        <router-link to="/students">
                            <a class="mdl-navigation__link">Studenti</a>
                        </router-link>
                        <router-link to="/teacher">
                            <a class="mdl-navigation__link">Predavaci</a>
                        </router-link>
                        <router-link to="/info">
                            <a class="mdl-navigation__link">Informacije</a>
                        </router-link>
                        <router-link to="/contact">
                            <a class="mdl-navigation__link">Kontakt</a>
                        </router-link>
                        <router-link to="/login">
                            <a class="mdl-navigation__link">Prijavi se</a>
                        </router-link>
                    </nav>
                </div>
            </header>
            <router-view></router-view>

            <footer class="mdl-mini-footer">
                <div style="width: 100%;">
                    <ul class="mdl-mini-footer__link-list">
                        <li style="width: 100%; text-align: center;"><a href="http://appsteam.vtsnis.edu.rs/"><img class="img-logo" src="images/vts-apps-team.png" alt=""></a></li>
                    </ul>
                </div>
            </footer>
        </div>

        <script src="/js/app.js"></script>
        <script
                src="https://code.jquery.com/jquery-3.2.1.min.js"
                integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
                crossorigin="anonymous"></script>
    </body>


</html>
