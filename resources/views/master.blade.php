<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>My app</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="/css/app.css">

        <!-- Styles -->
        
    </head>
    <body>
        <div id="app">
            
            <router-link to="/">Home</router-link>
            <router-link to="/about">About</router-link>

            <router-view></router-view>

        </div>
        <script src="/js/app.js"></script>
    </body>


</html>
