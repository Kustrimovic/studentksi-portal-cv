import VueRouter from 'vue-router';

let routes = [
	{
		path: '/',
		component: require('./views/home/Home')
	},	
	{
		path: '/training',
		component: require('./views/training/Training')
	},
	
	{
		path: '/students',
		component: require('./views/students/Students')
	},
	{
		path: '/info',
		component: require('./views/info/Info')
	},
	{
		path: '/contact',
		component: require('./views/contact/Contact')
	},
	{
		path: '/login',
		component: require('./views/login/Login')
	},
	{
		path: '/teacher',
		component: require('./views/teacher/Teacher')
	},
	{
		path: '/single-view-training',
		component: require('./views/single-view-training/single-view-training')
	},
	{
		path: '/single-view-home',
		component: require('./views/single-view-home/single-view-home')
	},
	// ADMIN
	{
		path: '/user-list',
		component: require('./views/admin/user-list/user-list')
	},
	{
		path: '/list-of-jobs',
		component: require('./views/admin/list-of-jobs/list-of-jobs')
	},
];

export default new VueRouter({
	routes,

	linkActiveClass: 'is-active'
});